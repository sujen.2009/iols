-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2016 at 09:26 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iols`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

DROP TABLE IF EXISTS `tbl_categories`;
CREATE TABLE IF NOT EXISTS `tbl_categories` (
`category_id` int(11) NOT NULL,
  `category_title` varchar(250) NOT NULL,
  `category_parent` int(11) DEFAULT NULL,
  `category_updated_date` datetime NOT NULL,
  `category_added_date` datetime NOT NULL,
  `category_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`category_id`, `category_title`, `category_parent`, `category_updated_date`, `category_added_date`, `category_status`) VALUES
(3, 'Pavers', NULL, '2016-09-10 06:38:08', '2016-09-10 06:38:08', 1),
(4, 'Retaining walls', NULL, '2016-09-10 07:11:07', '2016-09-10 06:38:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_features`
--

DROP TABLE IF EXISTS `tbl_category_features`;
CREATE TABLE IF NOT EXISTS `tbl_category_features` (
`feature_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `feature_title` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `feature_controltype` int(11) NOT NULL,
  `feature_controlopts` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_category_features`:
--   `category_id`
--       `tbl_categories` -> `category_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_info`
--

DROP TABLE IF EXISTS `tbl_contact_info`;
CREATE TABLE IF NOT EXISTS `tbl_contact_info` (
`contact_id` int(11) NOT NULL,
  `fullname` varchar(260) NOT NULL,
  `address` varchar(260) NOT NULL,
  `contactno` varchar(260) NOT NULL,
  `email_address` varchar(260) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact_info`
--

INSERT INTO `tbl_contact_info` (`contact_id`, `fullname`, `address`, `contactno`, `email_address`, `added_date`, `status`) VALUES
(2, 'Test test 1', 'test', 'test', 'a@a.com', '2016-08-16 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courier`
--

DROP TABLE IF EXISTS `tbl_courier`;
CREATE TABLE IF NOT EXISTS `tbl_courier` (
`courier_id` int(11) NOT NULL,
  `courier_regions` varchar(200) NOT NULL,
  `courier_charge` varchar(200) NOT NULL,
  `courier_added_date` datetime NOT NULL,
  `courier_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_delivery`
--

DROP TABLE IF EXISTS `tbl_delivery`;
CREATE TABLE IF NOT EXISTS `tbl_delivery` (
`delivery_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `delivery_charge` int(11) NOT NULL,
  `delivery_comment` text NOT NULL,
  `delivery_started_date` datetime NOT NULL,
  `delivery_completed_date` datetime NOT NULL,
  `delivery_added_date` datetime NOT NULL,
  `delivery_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delivered_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_delivery`:
--   `order_id`
--       `tbl_order` -> `order_id`
--   `courier_id`
--       `tbl_courier` -> `courier_id`
--   `delivered_by`
--       `tbl_users` -> `user_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

DROP TABLE IF EXISTS `tbl_images`;
CREATE TABLE IF NOT EXISTS `tbl_images` (
`image_id` int(11) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `image_thumb_name` varchar(200) NOT NULL,
  `image_primary_id` int(11) NOT NULL,
  `image_type` varchar(200) NOT NULL,
  `image_added_date` varchar(200) NOT NULL,
  `image_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_images`
--

INSERT INTO `tbl_images` (`image_id`, `image_name`, `image_thumb_name`, `image_primary_id`, `image_type`, `image_added_date`, `image_status`) VALUES
(2, '1471095310_6.png', '', 2, 'page', '2016-08-13 03:35:10', 1),
(3, '1471095456_6.png', '', 3, 'page', '2016-08-13 03:37:36', 1),
(5, '1473484268_thumb_retaining_wall_stone5.png', '1', 4, 'category', '2016-09-10 07:11:08', 1),
(6, '1473488030_blue_limestone_honed_2_4_2.jpg', '', 4, 'product', '2016-09-10 08:13:50', 1),
(7, '1473488383_thumb_blue_limestone_honed_2_4_2.jpg', '1', 4, 'product', '2016-09-10 08:19:43', 1),
(8, '1473495092_thumb_misty_grey_1_6.jpg', '1', 9, 'product', '2016-09-10 10:11:32', 1),
(9, '1473495107_thumb_ivory_20_resized_5.jpg', '1', 8, 'product', '2016-09-10 10:11:47', 1),
(10, '1473495114_thumb_ew6uylzbppmyvhjjkdxxp_nrdddhoiojf-2b_dn-bobbfzzmaftlarhzrgg8latltuwubg_s2048.jpg', '1', 7, 'product', '2016-09-10 10:11:54', 1),
(11, '1473495123_thumb_4ihfdvuw_01-so3b7efhbm2g1_4rlqsgcklv0_dbfzg5_ejypthgii4bfuunwkvwc1iw_w_s2048.jpg', '1', 6, 'product', '2016-09-10 10:12:03', 1),
(12, '1473495130_thumb_1silver_grey_granite_step_top_with_background.jpg', '1', 5, 'product', '2016-09-10 10:12:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
`menu_id` int(11) NOT NULL,
  `menu_parentid` int(11) DEFAULT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_link` varchar(250) NOT NULL,
  `menu_pageid` int(11) DEFAULT NULL,
  `menu_order` int(11) NOT NULL,
  `menu_status` int(11) NOT NULL,
  `menu_updated_date` datetime NOT NULL,
  `menu_added_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_menu`:
--   `menu_parentid`
--       `tbl_menu` -> `menu_id`
--   `menu_pageid`
--       `tbl_pages` -> `page_id`
--

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `menu_parentid`, `menu_title`, `menu_link`, `menu_pageid`, `menu_order`, `menu_status`, `menu_updated_date`, `menu_added_date`) VALUES
(2, NULL, '', '', 3, 2, 1, '2016-09-16 10:26:02', '2016-09-16 10:26:02'),
(3, NULL, 'Gallery', 'http://localhost/ioms_git/gallery', NULL, 3, 1, '2016-09-16 11:35:44', '2016-09-16 10:27:04'),
(7, 3, 'Test link', 'http://localhost/ioms_git/', NULL, 1, 1, '2016-09-16 03:14:25', '2016-09-16 03:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE IF NOT EXISTS `tbl_order` (
`order_id` int(11) NOT NULL,
  `order_placed_by` varchar(255) NOT NULL,
  `contact_no` varchar(250) NOT NULL,
  `total_price` double NOT NULL,
  `identification_no` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_detail`
--

DROP TABLE IF EXISTS `tbl_order_detail`;
CREATE TABLE IF NOT EXISTS `tbl_order_detail` (
`order_detail_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_price` double NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_order_detail`:
--   `order_id`
--       `tbl_order` -> `order_id`
--   `product_id`
--       `tbl_products` -> `product_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

DROP TABLE IF EXISTS `tbl_pages`;
CREATE TABLE IF NOT EXISTS `tbl_pages` (
`page_id` int(11) NOT NULL,
  `page_title` varchar(200) NOT NULL,
  `page_description` longtext NOT NULL,
  `page_short_description` longtext NOT NULL,
  `page_added_date` date NOT NULL,
  `page_updated_date` date NOT NULL,
  `page_status` tinyint(4) NOT NULL,
  `page_meta_tags` text NOT NULL,
  `page_meta_desc` text NOT NULL,
  `hometext` tinyint(4) NOT NULL,
  `page_type` enum('page','testimonial') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pages`
--

INSERT INTO `tbl_pages` (`page_id`, `page_title`, `page_description`, `page_short_description`, `page_added_date`, `page_updated_date`, `page_status`, `page_meta_tags`, `page_meta_desc`, `hometext`, `page_type`) VALUES
(3, 'Welcome page', 'test full content', 'test description', '2016-08-13', '2016-08-13', 1, 'test page', 'test page', 1, 'page'),
(5, 'About us', 'About us text', 'About us text', '2016-08-27', '2016-08-27', 1, 'about us', 'About us', 0, 'page');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
CREATE TABLE IF NOT EXISTS `tbl_products` (
`product_id` int(11) NOT NULL,
  `product_title` varchar(220) CHARACTER SET latin1 NOT NULL,
  `product_type` int(11) NOT NULL,
  `meta_description` text CHARACTER SET latin1 NOT NULL,
  `meta_tags` text CHARACTER SET latin1 NOT NULL,
  `product_price` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `product_unit` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Per Block',
  `product_brief_info` text CHARACTER SET latin1 NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `tbl_products`:
--   `product_type`
--       `tbl_categories` -> `category_id`
--   `added_by`
--       `tbl_users` -> `user_id`
--

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_title`, `product_type`, `meta_description`, `meta_tags`, `product_price`, `product_unit`, `product_brief_info`, `added_date`, `updated_date`, `added_by`, `status`) VALUES
(4, 'Blue Limestone Honed Bevel Edge', 3, 'Blue Limestone Honed Bevel Edge', 'Blue Limestone Honed Bevel Edge', '49', 'Per Block', '<p><span>Honed finish.</span><br /><br /><span>An ageless and aesthetically elegant choice for interior design with a smooth finish.</span></p>', '2016-09-10 08:19:43', '2016-09-10 08:13:50', 1, 1),
(5, 'Sesame White Granite Bullnose Pavers', 3, '', '', '["150","200"]', '["Per Block","Per Block"]', '<p><span>Flamed finish.</span></p>', '2016-09-19 07:15:34', '2016-09-10 09:26:48', 1, 1),
(6, 'Classic Travertine Bullnose Pavers', 3, 'Classic Travertine Bullnose Pavers', 'Classic Travertine Bullnose Pavers', '["100"]', '["per piece"]', '<p><span>Visually stunning</span><br /><span>Slip and salt resistant</span><br /><span>Reflects heat</span><br /><span>Non-abrasive</span><br /><span>Low maintenance</span></p>', '2016-09-19 03:09:12', '2016-09-10 09:28:55', 1, 1),
(7, 'Mongolian Basalt Black Pavers', 3, '', '', '["150"]', '["per block"]', '<p>The sleek and consistent dark hues of this natural stone provide a stunning contrast against landscapes and classic colour schemes.</p>\r\n<p>&nbsp;</p>', '2016-09-19 03:09:02', '2016-09-10 09:30:09', 1, 1),
(8, 'Misty Grey Marble Pavers', 3, 'Misty Grey Marble Pavers', 'Misty Grey Marble Pavers', '["100"]', '["per piece"]', '<p><span>Tumbled Edge</span><br /><br /><span>Imbue your property with the classical Grecian beauty of a bygone era!</span></p>', '2016-09-19 03:08:51', '2016-09-10 09:31:20', 1, 1),
(9, 'Polished Full Length Block', 3, 'Polished: 20.01 / 20.42 Full Length Block', 'Polished: 20.01 / 20.42 Full Length Block', '["150","200","1000"]', '["sq. meter","meter","Kilometer"]', '<p><span>Available in Three Compatible Colours!</span></p>', '2016-09-19 03:08:39', '2016-09-10 09:32:31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_feature_detail`
--

DROP TABLE IF EXISTS `tbl_product_feature_detail`;
CREATE TABLE IF NOT EXISTS `tbl_product_feature_detail` (
`detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `feature_title` varchar(250) NOT NULL,
  `feature_desc` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_product_feature_detail`:
--   `feature_id`
--       `tbl_category_features` -> `feature_id`
--   `product_id`
--       `tbl_products` -> `product_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

DROP TABLE IF EXISTS `tbl_slider`;
CREATE TABLE IF NOT EXISTS `tbl_slider` (
`slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_caption` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
`user_id` int(11) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_fullname` varchar(250) NOT NULL,
  `user_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_name`, `user_password`, `user_fullname`, `user_status`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_category_features`
--
ALTER TABLE `tbl_category_features`
 ADD PRIMARY KEY (`feature_id`), ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `tbl_contact_info`
--
ALTER TABLE `tbl_contact_info`
 ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_courier`
--
ALTER TABLE `tbl_courier`
 ADD PRIMARY KEY (`courier_id`);

--
-- Indexes for table `tbl_delivery`
--
ALTER TABLE `tbl_delivery`
 ADD PRIMARY KEY (`delivery_id`), ADD KEY `order_id` (`order_id`), ADD KEY `courier_id` (`courier_id`), ADD KEY `delivered_by` (`delivered_by`);

--
-- Indexes for table `tbl_images`
--
ALTER TABLE `tbl_images`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
 ADD PRIMARY KEY (`menu_id`), ADD KEY `menu_parentid` (`menu_parentid`), ADD KEY `menu_pageid` (`menu_pageid`), ADD KEY `menu_parentid_2` (`menu_parentid`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
 ADD PRIMARY KEY (`order_detail_id`), ADD KEY `order_id` (`order_id`), ADD KEY `order_id_2` (`order_id`), ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
 ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
 ADD PRIMARY KEY (`product_id`), ADD KEY `product_type` (`product_type`), ADD KEY `added_by` (`added_by`);

--
-- Indexes for table `tbl_product_feature_detail`
--
ALTER TABLE `tbl_product_feature_detail`
 ADD PRIMARY KEY (`detail_id`), ADD KEY `feature_id` (`feature_id`), ADD KEY `item_id` (`product_id`), ADD KEY `feature_id_2` (`feature_id`), ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_category_features`
--
ALTER TABLE `tbl_category_features`
MODIFY `feature_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contact_info`
--
ALTER TABLE `tbl_contact_info`
MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_courier`
--
ALTER TABLE `tbl_courier`
MODIFY `courier_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_delivery`
--
ALTER TABLE `tbl_delivery`
MODIFY `delivery_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_images`
--
ALTER TABLE `tbl_images`
MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_product_feature_detail`
--
ALTER TABLE `tbl_product_feature_detail`
MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_category_features`
--
ALTER TABLE `tbl_category_features`
ADD CONSTRAINT `tbl_category_features_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`category_id`);

--
-- Constraints for table `tbl_delivery`
--
ALTER TABLE `tbl_delivery`
ADD CONSTRAINT `tbl_delivery_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`order_id`),
ADD CONSTRAINT `tbl_delivery_ibfk_2` FOREIGN KEY (`courier_id`) REFERENCES `tbl_courier` (`courier_id`),
ADD CONSTRAINT `tbl_delivery_ibfk_3` FOREIGN KEY (`delivered_by`) REFERENCES `tbl_users` (`user_id`);

--
-- Constraints for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
ADD CONSTRAINT `tbl_menu_ibfk_1` FOREIGN KEY (`menu_parentid`) REFERENCES `tbl_menu` (`menu_id`),
ADD CONSTRAINT `tbl_menu_ibfk_2` FOREIGN KEY (`menu_pageid`) REFERENCES `tbl_pages` (`page_id`);

--
-- Constraints for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
ADD CONSTRAINT `tbl_order_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_order_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`product_id`);

--
-- Constraints for table `tbl_products`
--
ALTER TABLE `tbl_products`
ADD CONSTRAINT `tbl_products_ibfk_1` FOREIGN KEY (`product_type`) REFERENCES `tbl_categories` (`category_id`),
ADD CONSTRAINT `tbl_products_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `tbl_users` (`user_id`);

--
-- Constraints for table `tbl_product_feature_detail`
--
ALTER TABLE `tbl_product_feature_detail`
ADD CONSTRAINT `tbl_product_feature_detail_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `tbl_category_features` (`feature_id`),
ADD CONSTRAINT `tbl_product_feature_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
