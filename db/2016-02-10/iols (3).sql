-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2016 at 10:04 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iols`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

DROP TABLE IF EXISTS `tbl_categories`;
CREATE TABLE IF NOT EXISTS `tbl_categories` (
`category_id` int(11) NOT NULL,
  `category_title` varchar(250) NOT NULL,
  `category_parent` int(11) DEFAULT NULL,
  `category_updated_date` datetime NOT NULL,
  `category_added_date` datetime NOT NULL,
  `category_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`category_id`, `category_title`, `category_parent`, `category_updated_date`, `category_added_date`, `category_status`) VALUES
(3, 'Pavers', NULL, '2016-09-10 06:38:08', '2016-09-10 06:38:08', 1),
(4, 'Retaining walls', NULL, '2016-09-10 07:11:07', '2016-09-10 06:38:35', 1),
(5, 'Test cat', 3, '2016-09-01 00:00:00', '2016-09-02 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_features`
--

DROP TABLE IF EXISTS `tbl_category_features`;
CREATE TABLE IF NOT EXISTS `tbl_category_features` (
`feature_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `feature_title` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `feature_controltype` int(11) NOT NULL,
  `feature_controlopts` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_info`
--

DROP TABLE IF EXISTS `tbl_contact_info`;
CREATE TABLE IF NOT EXISTS `tbl_contact_info` (
`contact_id` int(11) NOT NULL,
  `fullname` varchar(260) NOT NULL,
  `address` varchar(260) NOT NULL,
  `contactno` varchar(260) NOT NULL,
  `email_address` varchar(260) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact_info`
--

INSERT INTO `tbl_contact_info` (`contact_id`, `fullname`, `address`, `contactno`, `email_address`, `added_date`, `status`) VALUES
(2, 'Test test 1', 'test', 'test', 'a@a.com', '2016-08-16 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courier`
--

DROP TABLE IF EXISTS `tbl_courier`;
CREATE TABLE IF NOT EXISTS `tbl_courier` (
`courier_id` int(11) NOT NULL,
  `courier_regions` varchar(200) NOT NULL,
  `courier_charge` varchar(200) NOT NULL,
  `courier_added_date` datetime NOT NULL,
  `courier_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_delivery`
--

DROP TABLE IF EXISTS `tbl_delivery`;
CREATE TABLE IF NOT EXISTS `tbl_delivery` (
`delivery_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `delivery_charge` int(11) NOT NULL,
  `delivery_comment` text NOT NULL,
  `delivery_started_date` datetime NOT NULL,
  `delivery_completed_date` datetime NOT NULL,
  `delivery_added_date` datetime NOT NULL,
  `delivery_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delivered_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

DROP TABLE IF EXISTS `tbl_images`;
CREATE TABLE IF NOT EXISTS `tbl_images` (
`image_id` int(11) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `image_thumb_name` varchar(200) NOT NULL,
  `image_primary_id` int(11) NOT NULL,
  `image_type` varchar(200) NOT NULL,
  `image_added_date` varchar(200) NOT NULL,
  `image_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_images`
--

INSERT INTO `tbl_images` (`image_id`, `image_name`, `image_thumb_name`, `image_primary_id`, `image_type`, `image_added_date`, `image_status`) VALUES
(2, '1471095310_6.png', '', 2, 'page', '2016-08-13 03:35:10', 1),
(3, '1471095456_6.png', '', 3, 'page', '2016-08-13 03:37:36', 1),
(5, '1473484268_thumb_retaining_wall_stone5.png', '1', 4, 'category', '2016-09-10 07:11:08', 1),
(6, '1473488030_blue_limestone_honed_2_4_2.jpg', '', 4, 'product', '2016-09-10 08:13:50', 1),
(7, '1473488383_thumb_blue_limestone_honed_2_4_2.jpg', '1', 4, 'product', '2016-09-10 08:19:43', 1),
(8, '1473495092_thumb_misty_grey_1_6.jpg', '1', 9, 'product', '2016-09-10 10:11:32', 1),
(9, '1473495107_thumb_ivory_20_resized_5.jpg', '1', 8, 'product', '2016-09-10 10:11:47', 1),
(10, '1473495114_thumb_ew6uylzbppmyvhjjkdxxp_nrdddhoiojf-2b_dn-bobbfzzmaftlarhzrgg8latltuwubg_s2048.jpg', '1', 7, 'product', '2016-09-10 10:11:54', 1),
(11, '1473495123_thumb_4ihfdvuw_01-so3b7efhbm2g1_4rlqsgcklv0_dbfzg5_ejypthgii4bfuunwkvwc1iw_w_s2048.jpg', '1', 6, 'product', '2016-09-10 10:12:03', 1),
(12, '1473495130_thumb_1silver_grey_granite_step_top_with_background.jpg', '1', 5, 'product', '2016-09-10 10:12:10', 1),
(13, '1silver_grey_granite_step_top_with_background.jpg', '', 1, 'slider', '2016-10-01 08:43:30', 1),
(14, '4ihfdvuw_01-so3b7efhbm2g1_4rlqsgcklv0_dbfzg5_ejypthgii4bfuunwkvwc1iw_w_s2048.jpg', '', 2, 'slider', '2016-10-01 08:43:30', 1),
(15, '14292474_1711605242501586_4044202914004074220_n.jpg', '', 3, 'slider', '2016-10-01 08:43:30', 1),
(16, '14355067_1693733380948125_1326763895171954302_n.jpg', '', 4, 'slider', '2016-10-01 08:43:31', 1),
(17, 'ew6uylzbppmyvhjjkdxxp_nrdddhoiojf-2b_dn-bobbfzzmaftlarhzrgg8latltuwubg_s2048.jpg', '', 5, 'slider', '2016-10-01 08:47:22', 1),
(18, 'banner.png', '', 6, 'slider', '2016-10-01 08:59:58', 1),
(19, 'banner.png', '', 7, 'slider', '2016-10-01 09:00:47', 1),
(23, '1475393927_thumb_4ihfdvuw_01-so3b7efhbm2g1_4rlqsgcklv0_dbfzg5_ejypthgii4bfuunwkvwc1iw_w_s2048.jpg', '1', 10, 'page', '2016-10-02 09:38:47', 1),
(24, '1475393937_thumb_4ihfdvuw_01-so3b7efhbm2g1_4rlqsgcklv0_dbfzg5_ejypthgii4bfuunwkvwc1iw_w_s2048.jpg', '1', 9, 'page', '2016-10-02 09:38:57', 1),
(25, '1475394191_thumb_1silver_grey_granite_step_top_with_background.jpg', '1', 12, 'page', '2016-10-02 09:43:11', 1),
(26, '1475394210_thumb_4ihfdvuw_01-so3b7efhbm2g1_4rlqsgcklv0_dbfzg5_ejypthgii4bfuunwkvwc1iw_w_s2048.jpg', '1', 13, 'page', '2016-10-02 09:43:30', 1),
(27, '1475394270_thumb_1silver_grey_granite_step_top_with_background.jpg', '1', 14, 'page', '2016-10-02 09:44:30', 1),
(28, '1475394285_thumb_1silver_grey_granite_step_top_with_background.jpg', '1', 15, 'page', '2016-10-02 09:44:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
`menu_id` int(11) NOT NULL,
  `menu_parentid` int(11) DEFAULT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_link` varchar(250) NOT NULL,
  `menu_pageid` int(11) DEFAULT NULL,
  `menu_order` int(11) NOT NULL,
  `menu_status` int(11) NOT NULL,
  `menu_updated_date` datetime NOT NULL,
  `menu_added_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `menu_parentid`, `menu_title`, `menu_link`, `menu_pageid`, `menu_order`, `menu_status`, `menu_updated_date`, `menu_added_date`) VALUES
(8, NULL, 'Products', 'http://localhost/ioms_git/products', NULL, 5, 1, '2016-09-25 04:27:55', '2016-09-25 04:27:55'),
(10, NULL, 'Pavers', 'http://localhost/ioms_git/category/Pavers', NULL, 6, 1, '2016-09-25 04:53:00', '2016-09-25 04:53:00'),
(11, NULL, '', '', 5, 4, 1, '2016-09-25 04:53:13', '2016-09-25 04:53:13'),
(13, NULL, 'Contact us', 'http://localhost/ioms_git/contact', NULL, 8, 1, '2016-10-02 07:59:30', '2016-10-02 07:59:30'),
(14, NULL, 'Reviews', 'http://localhost/ioms_git/testimonial', NULL, 7, 1, '2016-10-02 10:02:11', '2016-10-02 10:02:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE IF NOT EXISTS `tbl_order` (
`order_id` int(11) NOT NULL,
  `order_placed_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_price` double NOT NULL,
  `shipping_address` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `country` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `paymentMethod` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `order_placed_by`, `contact_no`, `total_price`, `shipping_address`, `email`, `created_date`, `status`, `country`, `state`, `paymentMethod`) VALUES
(1, 'Sujendra', '234234', 300, 'test', 'test@test.com', '0000-00-00 00:00:00', 0, 'Australia', 'Australia Capital Territory', 1),
(2, 'Sujendra', '234234', 300, 'test', 'test@test.com', '2016-09-25 04:05:11', 0, 'Australia', 'Australia Capital Territory', 1),
(3, 'Sujendra', '234234', 300, 'test', 'test@test.com', '2016-09-25 04:39:06', 0, 'Australia', 'Australia Capital Territory', 1),
(4, 'Sujendra shrestha', '9860026655', 300, 'test test', 'tetst@test.com', '2016-09-25 04:42:04', 0, 'Australia', 'Australia Capital Territory', 1),
(5, 'Sujendra shrestha', '9860026655', 300, 'Test test', 'test@test.com', '2016-09-25 04:43:00', 0, 'Australia', 'New South Wales', 1),
(6, 'Sujen', 'test', 1500, 'test', 'test', '2016-09-25 05:14:29', 0, 'Australia', 'New South Wales', 1),
(7, 'Sujendra', '234', 3000, 'sdfasd', 'dsf', '2016-09-25 05:18:32', 0, 'Australia', 'Queensland', 1),
(8, 'Sujendra', '234', 3000, 'sdfasd', 'dsf', '2016-09-25 05:19:40', 0, 'Australia', 'Queensland', 1),
(9, 'Sujendra', '234', 3000, 'sdfasd', 'dsf', '2016-09-25 05:49:01', 0, 'Australia', 'Queensland', 1),
(12, 'ef', 'ewwe', 450, 'wer', 'sfd', '2016-09-25 06:12:55', 0, 'Australia', 'Victoria', 1),
(13, 'test', 'test', 2000, 'test', 'test', '2016-09-25 06:16:17', 0, 'Australia', 'New South Wales', 1),
(14, 'test', 'test', 2000, 'test', 'test', '2016-09-25 06:20:49', 0, 'Australia', 'New South Wales', 1),
(15, 'test', 'test', 2000, 'test', 'test', '2016-09-25 06:25:23', 0, 'Australia', 'New South Wales', 1),
(16, 'test', 'test', 2000, 'test', 'test', '2016-09-25 06:25:55', 0, 'Australia', 'New South Wales', 1),
(17, 'test', 'test', 2000, 'test', 'test', '2016-09-25 06:26:20', 0, 'Australia', 'New South Wales', 1),
(21, 'Sujendra', '1239878787', 9000, 'Test tetw', 'sujen.2009@gmail.com', '2016-09-24 19:45:30', 0, 'Australia', 'Australia Capital Territory', 1),
(22, 'Sujendra shrestha', '123123123', 600, 'Lubhoo', 'sujen.2009@gmail.com', '2016-09-30 06:21:20', 0, 'Australia', 'Australia Capital Territory', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_detail`
--

DROP TABLE IF EXISTS `tbl_order_detail`;
CREATE TABLE IF NOT EXISTS `tbl_order_detail` (
`order_detail_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_price` double NOT NULL,
  `product_unit` varchar(200) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_detail`
--

INSERT INTO `tbl_order_detail` (`order_detail_id`, `order_id`, `product_id`, `product_price`, `product_unit`, `product_quantity`, `order_status`, `created_date`) VALUES
(1, 3, 5, 150, 'Block', 2, 0, '2016-09-25 08:24:06'),
(2, 4, 5, 150, 'Block', 2, 0, '2016-09-25 08:27:04'),
(3, 5, 7, 150, 'block', 2, 0, '2016-09-25 08:28:00'),
(4, 6, 9, 150, 'sq. meter', 10, 0, '2016-09-25 08:59:29'),
(5, 7, 9, 150, 'sq. meter', 20, 0, '2016-09-25 09:03:32'),
(6, 8, 9, 150, 'sq. meter', 20, 0, '2016-09-25 09:04:40'),
(8, 13, 6, 100, 'piece', 20, 0, '2016-09-25 10:01:17'),
(9, 14, 6, 100, 'piece', 20, 0, '2016-09-25 10:05:50'),
(10, 15, 6, 100, 'piece', 20, 0, '2016-09-25 10:10:23'),
(11, 16, 6, 100, 'piece', 20, 0, '2016-09-25 10:10:55'),
(12, 17, 6, 100, 'piece', 20, 0, '2016-09-25 10:11:20'),
(16, 21, 6, 100, 'piece', 20, 0, '2016-09-25 11:30:30'),
(17, 21, 4, 100, 'piece', 10, 0, '2016-09-25 11:30:30'),
(18, 21, 5, 150, 'Block', 20, 0, '2016-09-25 11:30:30'),
(19, 21, 9, 150, 'sq. meter', 20, 0, '2016-09-25 11:30:30'),
(20, 22, 7, 150, 'block', 4, 0, '2016-09-30 10:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

DROP TABLE IF EXISTS `tbl_pages`;
CREATE TABLE IF NOT EXISTS `tbl_pages` (
`page_id` int(11) NOT NULL,
  `page_title` varchar(200) NOT NULL,
  `page_description` longtext NOT NULL,
  `page_short_description` longtext NOT NULL,
  `page_added_date` date NOT NULL,
  `page_updated_date` date NOT NULL,
  `page_status` tinyint(4) NOT NULL,
  `page_meta_tags` text NOT NULL,
  `page_meta_desc` text NOT NULL,
  `hometext` tinyint(4) NOT NULL,
  `page_type` enum('page','testimonial','news') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pages`
--

INSERT INTO `tbl_pages` (`page_id`, `page_title`, `page_description`, `page_short_description`, `page_added_date`, `page_updated_date`, `page_status`, `page_meta_tags`, `page_meta_desc`, `hometext`, `page_type`) VALUES
(3, 'Welcome', '<p><span>UNIQUE STYLING&hellip;&hellip;WITH VALUE is our aim at Inside Out Living Solutions!</span><br /><br /><span>Our aim at Inside Out Living Solutions is to provide&nbsp;</span><span class="text_exposed_show">you with products that are not only unique but&hellip;with a sense of style that keeps you a step ahead of the crowd with competitive prices.<br /><br />We have been a wholesale business for the last 7 years, selling landscape products ranging from concrete pavers, eye catching natural stone and Terrazo that will become an eye catching feature in your home.<br /><br />We are fortunate to have the SA distribution rights with National Masonry. Who are leading the way through the eastern states, with their large range of concrete pavers and retaining walls.<br /><br />We have developed strong friendships with our re-sellers working hard to provide you a range of products that will create your home a place of individual style and tranquillity.</span></p>', '<p><span>UNIQUE STYLING&hellip;&hellip;WITH VALUE is our aim at Inside Out Living Solutions!</span><br /><br /><span>Our aim at Inside Out Living Solutions is to provide&nbsp;</span><span class="text_exposed_show">you with products that are not only unique but&hellip;with a sense of style that keeps you a step ahead of the crowd with competitive prices.<br /><br />We have been a wholesale business for the last 7 years, selling landscape products ranging from concrete pavers, eye catching natural stone and Terrazo that will become an eye catching feature in your home.<br /><br /><br /></span></p>', '2016-08-13', '2016-10-02', 1, 'test page', 'test page', 1, 'page'),
(5, 'About us', '<p><span>UNIQUE STYLING&hellip;&hellip;WITH VALUE is our aim at Inside Out Living Solutions!</span><br /><br /><span>Our aim at Inside Out Living Solutions is to provide&nbsp;</span><span class="text_exposed_show">you with products that are not only unique but&hellip;with a sense of style that keeps you a step ahead of the crowd with competitive prices.<br /><br />We have been a wholesale business for the last 7 years, selling landscape products ranging from concrete pavers, eye catching natural stone and Terrazo that will become an eye catching feature in your home.<br /><br />We are fortunate to have the SA distribution rights with National Masonry. Who are leading the way through the eastern states, with their large range of concrete pavers and retaining walls.<br /><br />We have developed strong friendships with our re-sellers working hard to provide you a range of products that will create your home a place of individual style and tranquillity.</span></p>', '<p><span>UNIQUE STYLING&hellip;&hellip;WITH VALUE is our aim at Inside Out Living Solutions!</span><br /><br /><span>Our aim at Inside Out Living Solutions is to provide&nbsp;</span><span class="text_exposed_show">you with products that are not only unique but&hellip;with a sense of style that keeps you a step ahead of the crowd with competitive prices.<br /><br />We have been a wholesale business for the last 7 years, selling landscape products ranging from concrete pavers, eye catching natural stone and Terrazo that will become an eye catching feature in your home.<br /><br />We are fortunate to have the SA distribution rights with National Masonry. Who are leading the way through the eastern states, with their large range of concrete pavers and retaining walls.<br /><br />We have developed strong friendships with our re-sellers working hard to provide you a range of products that will create your home a place of individual style and tranquillity.</span></p>', '2016-08-27', '2016-10-02', 1, 'about us', 'About us', 0, 'page'),
(6, 'Contact Us', '<p>This is a contact us page</p>', '<p>This is a contact us page</p>', '2016-09-25', '2016-09-25', 1, 'contact us', 'contact us', 0, 'page'),
(7, 'New website Launched re', '<p>This is a long description</p>', '<p>This is a short description</p>', '2016-10-01', '2016-10-01', 1, 'test', 'et', 0, 'news'),
(8, 'New product lunched', '<p>We have lunched new product today.</p>', '<p>We have new product lunched today.</p>', '2016-10-02', '2016-10-02', 1, 'product lunch', 'product lunch', 0, 'news'),
(12, 'Sujendra shrestha', '<p><span>LOVE when I call I get to speak to someone straight away. Never had to wait on phone more than 1/2min. VERY friendly. Great discounts! Cost is very important to us and the quality. We have advised for 1-2 weeks delivery, but it is delivered earlier than we expected. We will definitely recommend hunterstone. Thanks for this wonderful pavers!</span></p>', '', '2016-10-02', '2016-10-02', 1, '', '', 0, 'testimonial'),
(13, 'Suren Maharjan', '<p><span>When seeking for a prices for a new deal I approached several companies. All claimed to have the best rates and happily informed be on the phone. But getting them to confirm by email or letter was like "nailing a jelly to the ceiling." I always ask for conformation to avoid the "I don''t know who told you that!" later on response when charged differently, which has happened. Hunterstone had no problems in confirming and the prices were the best for my needs, the price is very affordable and comes with quality products. Also I didn''t have to wait an age to speak to someone which was nice and those I spoke to knew what they were talking about.Keep it up and continue to produce this type of product, it is beautiful!</span></p>', '', '2016-10-02', '2016-10-02', 1, '', '', 0, 'testimonial'),
(14, 'Thomas wayne', '<p><span>When seeking for a prices for a new deal I approached several companies. All claimed to have the best rates and happily informed be on the phone. But getting them to confirm by email or letter was like "nailing a jelly to the ceiling." I always ask for conformation to avoid the "I don''t know who told you that!" later on response when charged differently, which has happened. Hunterstone had no problems in confirming and the prices were the best for my needs, the price is very affordable and comes with quality products. Also I didn''t have to wait an age to speak to someone which was nice and those I spoke to knew what they were talking about.Keep it up and continue to produce this type of product, it is beautiful!</span></p>', '', '2016-10-02', '2016-10-02', 1, '', '', 0, 'testimonial'),
(15, 'Clark Kent', '<p><span>When seeking for a prices for a new deal I approached several companies. All claimed to have the best rates and happily informed be on the phone. But getting them to confirm by email or letter was like "nailing a jelly to the ceiling." I always ask for conformation to avoid the "I don''t know who told you that!" later on response when charged differently, which has happened. Hunterstone had no problems in confirming and the prices were the best for my needs, the price is very affordable and comes with quality products. Also I didn''t have to wait an age to speak to someone which was nice and those I spoke to knew what they were talking about.Keep it up and continue to produce this type of product, it is beautiful!</span></p>', '', '2016-10-02', '2016-10-02', 1, '', '', 0, 'testimonial');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
CREATE TABLE IF NOT EXISTS `tbl_products` (
`product_id` int(11) NOT NULL,
  `product_title` varchar(220) CHARACTER SET latin1 NOT NULL,
  `product_type` int(11) NOT NULL,
  `meta_description` text CHARACTER SET latin1 NOT NULL,
  `meta_tags` text CHARACTER SET latin1 NOT NULL,
  `product_price` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `product_unit` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Per Block',
  `product_brief_info` text CHARACTER SET latin1 NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_title`, `product_type`, `meta_description`, `meta_tags`, `product_price`, `product_unit`, `product_brief_info`, `added_date`, `updated_date`, `added_by`, `status`) VALUES
(4, 'Blue Limestone Honed Bevel Edge', 3, 'Blue Limestone Honed Bevel Edge', 'Blue Limestone Honed Bevel Edge', '["100"]', '["piece"]', '<p><span>Honed finish.</span><br /><br /><span>An ageless and aesthetically elegant choice for interior design with a smooth finish.</span></p>', '2016-09-24 08:54:35', '2016-09-10 08:13:50', 1, 1),
(5, 'Sesame White Granite Bullnose Pavers', 3, '', '', '["150"]', '["Block"]', '<p><span>Flamed finish.</span></p>', '2016-09-24 08:54:12', '2016-09-10 09:26:48', 1, 1),
(6, 'Classic Travertine Bullnose Pavers', 3, 'Classic Travertine Bullnose Pavers', 'Classic Travertine Bullnose Pavers', '["100"]', '["piece"]', '<p><span>Visually stunning</span><br /><span>Slip and salt resistant</span><br /><span>Reflects heat</span><br /><span>Non-abrasive</span><br /><span>Low maintenance</span></p>', '2016-09-24 08:53:37', '2016-09-10 09:28:55', 1, 1),
(7, 'Mongolian Basalt Black Pavers', 3, '', '', '["150"]', '["block"]', '<p>The sleek and consistent dark hues of this natural stone provide a stunning contrast against landscapes and classic colour schemes.</p>\r\n<p>&nbsp;</p>', '2016-09-24 08:53:26', '2016-09-10 09:30:09', 1, 1),
(8, 'Misty Grey Marble Pavers', 3, 'Misty Grey Marble Pavers', 'Misty Grey Marble Pavers', '["100"]', '["piece"]', '<p><span>Tumbled Edge</span><br /><br /><span>Imbue your property with the classical Grecian beauty of a bygone era!</span></p>', '2016-09-24 08:53:19', '2016-09-10 09:31:20', 1, 1),
(9, 'Polished Full Length Block', 3, 'Polished: 20.01 / 20.42 Full Length Block', 'Polished: 20.01 / 20.42 Full Length Block', '["150","200","1000"]', '["sq. meter","meter","Kilometer"]', '<p><span>Available in Three Compatible Colours!</span></p>', '2016-09-19 03:08:39', '2016-09-10 09:32:31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_feature_detail`
--

DROP TABLE IF EXISTS `tbl_product_feature_detail`;
CREATE TABLE IF NOT EXISTS `tbl_product_feature_detail` (
`detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `feature_title` varchar(250) NOT NULL,
  `feature_desc` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

DROP TABLE IF EXISTS `tbl_slider`;
CREATE TABLE IF NOT EXISTS `tbl_slider` (
`slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_caption` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_title`, `slider_caption`, `status`) VALUES
(6, 'Banner image', 'Banner image', 1),
(7, 'Banner image 2', 'Banner image 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
`user_id` int(11) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_fullname` varchar(250) NOT NULL,
  `user_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_name`, `user_password`, `user_fullname`, `user_status`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_category_features`
--
ALTER TABLE `tbl_category_features`
 ADD PRIMARY KEY (`feature_id`), ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `tbl_contact_info`
--
ALTER TABLE `tbl_contact_info`
 ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_courier`
--
ALTER TABLE `tbl_courier`
 ADD PRIMARY KEY (`courier_id`);

--
-- Indexes for table `tbl_delivery`
--
ALTER TABLE `tbl_delivery`
 ADD PRIMARY KEY (`delivery_id`), ADD KEY `order_id` (`order_id`), ADD KEY `courier_id` (`courier_id`), ADD KEY `delivered_by` (`delivered_by`);

--
-- Indexes for table `tbl_images`
--
ALTER TABLE `tbl_images`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
 ADD PRIMARY KEY (`menu_id`), ADD KEY `menu_parentid` (`menu_parentid`), ADD KEY `menu_pageid` (`menu_pageid`), ADD KEY `menu_parentid_2` (`menu_parentid`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
 ADD PRIMARY KEY (`order_detail_id`), ADD KEY `order_id` (`order_id`), ADD KEY `order_id_2` (`order_id`), ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
 ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
 ADD PRIMARY KEY (`product_id`), ADD KEY `product_type` (`product_type`), ADD KEY `added_by` (`added_by`);

--
-- Indexes for table `tbl_product_feature_detail`
--
ALTER TABLE `tbl_product_feature_detail`
 ADD PRIMARY KEY (`detail_id`), ADD KEY `feature_id` (`feature_id`), ADD KEY `item_id` (`product_id`), ADD KEY `feature_id_2` (`feature_id`), ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_category_features`
--
ALTER TABLE `tbl_category_features`
MODIFY `feature_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contact_info`
--
ALTER TABLE `tbl_contact_info`
MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_courier`
--
ALTER TABLE `tbl_courier`
MODIFY `courier_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_delivery`
--
ALTER TABLE `tbl_delivery`
MODIFY `delivery_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_images`
--
ALTER TABLE `tbl_images`
MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_product_feature_detail`
--
ALTER TABLE `tbl_product_feature_detail`
MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_category_features`
--
ALTER TABLE `tbl_category_features`
ADD CONSTRAINT `tbl_category_features_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`category_id`);

--
-- Constraints for table `tbl_delivery`
--
ALTER TABLE `tbl_delivery`
ADD CONSTRAINT `tbl_delivery_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`order_id`),
ADD CONSTRAINT `tbl_delivery_ibfk_2` FOREIGN KEY (`courier_id`) REFERENCES `tbl_courier` (`courier_id`),
ADD CONSTRAINT `tbl_delivery_ibfk_3` FOREIGN KEY (`delivered_by`) REFERENCES `tbl_users` (`user_id`);

--
-- Constraints for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
ADD CONSTRAINT `tbl_menu_ibfk_1` FOREIGN KEY (`menu_parentid`) REFERENCES `tbl_menu` (`menu_id`),
ADD CONSTRAINT `tbl_menu_ibfk_2` FOREIGN KEY (`menu_pageid`) REFERENCES `tbl_pages` (`page_id`);

--
-- Constraints for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
ADD CONSTRAINT `tbl_order_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_order_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`product_id`);

--
-- Constraints for table `tbl_products`
--
ALTER TABLE `tbl_products`
ADD CONSTRAINT `tbl_products_ibfk_1` FOREIGN KEY (`product_type`) REFERENCES `tbl_categories` (`category_id`),
ADD CONSTRAINT `tbl_products_ibfk_2` FOREIGN KEY (`added_by`) REFERENCES `tbl_users` (`user_id`);

--
-- Constraints for table `tbl_product_feature_detail`
--
ALTER TABLE `tbl_product_feature_detail`
ADD CONSTRAINT `tbl_product_feature_detail_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `tbl_category_features` (`feature_id`),
ADD CONSTRAINT `tbl_product_feature_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
