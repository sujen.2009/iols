<?php

		$config = array(
                 'change_pass' => array(
                                    array(
                                            'field' => 'o_pass',
                                            'label' => 'Old password',
                                            'rules' => 'required|callback_check_old_pass'
                                         ),
                                    array(
                                            'field' => 'uname',
                                            'label' => 'Username',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'full_name',
                                            'label' => 'Fullname',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email address',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'n_pass',
                                            'label' => 'New Password',
                                            'rules' => 'min_length[5]|matches[r_pass]'
                                         ),
                                    array(
                                            'field' => 'r_pass',
                                            'label' => 'Retype password',
                                            'rules' => '|matches[n_pass]'
                                         )
                                   ),
                'user_login' => array(
                                    array(
                                            'field' => 'userid',
                                            'label' => 'User name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         )
                                   ),
                 'forgot_pass' => array(
                                    array(
                                            'field' => 'email',
                                            'label' => 'required',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'captcha',
                                            'label' => 'Word verification',
                                            'rules' => 'required|callback_check_captcha_recover'
                                         )
                                   ),
                 'update_user' => array(
                                    array(
                                            'field' => 'full_name',
                                            'label' => 'Full name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_email',
                                            'label' => 'required',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'user_name',
                                            'label' => 'User name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|callback_check_password'
                                         )
                                   ),
                                   'join_now' => array(
                                    array(
                                            'field' => 'name',
                                            'label' => 'required',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'captcha_code',
                                            'label' => 'Captcha',
                                            'rules' => 'required|callback_captcha_check'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email address',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'address',
                                            'label' => 'Address',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'comment',
                                            'label' => 'Comment',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'phone',
                                            'label' => 'Contact no.',
                                            'rules' => 'required'
                                         )
                                   ),
                 'send_mail' => array(
                                    array(
                                            'field' => 'contact',
                                            'label' => 'Contact',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'refno',
                                            'label' => 'Ad ID/Ref. no',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'fname',
                                            'label' => 'First name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'comment',
                                            'label' => 'Comment',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email address',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'lname',
                                            'label' => 'Lastname',
                                            'rules' => 'required'
                                         )
                                   ),
                 'add_new_country' => array(
                                    array(
                                            'field' => 'country_name',
                                            'label' => 'Country Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'page_title',
                                            'label' => 'Page title',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_meta_desc',
                                            'label' => 'Meta description',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_meta_tags',
                                            'label' => 'Meta tags',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_introduction',
                                            'label' => 'Country introduction',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_wtg',
                                            'label' => 'Where to go',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_extra_info',
                                            'label' => 'Extra introduction',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_gettingthere',
                                            'label' => 'Getting there',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_visa',
                                            'label' => 'Visa required',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_currency',
                                            'label' => 'Country currency',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_insurance',
                                            'label' => 'Insurance',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_health_issue',
                                            'label' => 'Health issue',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_culture_history',
                                            'label' => 'Culture history',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'country_faq',
                                            'label' => 'FAQ',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'image1',
                                            'label' => 'Image',
                                            'rules' => 'callback_validate_image'
                                         )
                                ),
                            'add_new_page' => array(
                                    array(
                                            'field' => 'page_short_description',
                                            'label' => 'Short description',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'page_title',
                                            'label' => 'Page title',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'page_meta_tags',
                                            'label' => 'Meta description',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'page_description',
                                            'label' => 'Page description',
                                            'rules' => 'required'
                                         )
                            )
                                    

               );

?>
