<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['product_detail/(:any)'] = "home/product_detail/$1";


$route['Userctrl/page/delete/(:any)'] = 'userctrl/delete_page/$1/page';
$route['userctrl/page/delete/(:any)'] = 'userctrl/delete_page/$1/page';


$route['Userctrl/testimonial/delete/(:any)'] = 'userctrl/delete_page/$1/testimonial';
$route['userctrl/testimonial/delete/(:any)'] = 'userctrl/delete_page/$1/testimonial';


$route['Userctrl/news/delete/(:any)'] = 'userctrl/delete_page/$1/news';
$route['userctrl/news/delete/(:any)'] = 'userctrl/delete_page/$1/news';


$route['Userctrl/page/add'] = 'userctrl/add_page';
$route['userctrl/page/add'] = 'userctrl/add_page';

$route['Userctrl/page/edit/(:any)'] = 'userctrl/edit_page/$1';
$route['userctrl/page/edit/(:any)'] = 'userctrl/edit_page/$1';
$route['userctrl/page/edit/(:any)/'] = 'userctrl/edit_page/$1';

$route['userctrl/page/list'] = 'userctrl/list_page';
$route['Userctrl/page/list'] = 'userctrl/list_page';

$route['Userctrl/product/delete/(:any)'] = 'userctrl/delete_product/$1';
$route['userctrl/product/delete/(:any)'] = 'userctrl/delete_product/$1';


$route['Userctrl/product/add'] = 'userctrl/add_product';
$route['userctrl/product/add'] = 'userctrl/add_product';

$route['Userctrl/product/edit/(:any)'] = 'userctrl/edit_product/$1';
$route['userctrl/product/edit/(:any)'] = 'userctrl/edit_product/$1';
$route['userctrl/product/edit/(:any)/'] = 'userctrl/edit_product/$1';

$route['userctrl/product/list'] = 'userctrl/list_product';
$route['Userctrl/product/list'] = 'userctrl/list_product';


$route['Userctrl/list_news'] = 'userctrl/list_page/news';
$route['userctrl/list_news'] = 'userctrl/list_page/news';

$route['Userctrl/news/list'] = 'userctrl/list_page/news';
$route['userctrl/news/list'] = 'userctrl/list_page/news';

$route['Userctrl/edit_news/(:any)'] = 'userctrl/edit_page/$1/news';
$route['userctrl/edit_news/(:any)'] = 'userctrl/edit_page/$1/news';

$route['Userctrl/news/add'] = 'userctrl/add_content/news';
$route['userctrl/news/add'] = 'userctrl/add_content/news';




$route['Userctrl/testimonial/list'] = 'userctrl/list_testimonial';
$route['userctrl/testimonial/list'] = 'userctrl/list_testimonial';

$route['Userctrl/testimonial/add'] = 'userctrl/add_content/testimonial';
$route['userctrl/testimonial/add'] = 'userctrl/add_content/testimonial';

$route['Userctrl/testimonial/edit/(:any)'] = 'userctrl/edit_testimonial/$1';
$route['userctrl/testimonial/edit/(:any)'] = 'userctrl/edit_testimonial/$1';


$route['Userctrl/category/edit/(uany)'] = 'userctrl/edit_category/$1';
$route['userctrl/category/edit/(:any)'] = 'userctrl/edit_category/$1';

$route['Userctrl/category/add'] = 'userctrl/add_category';
$route['userctrl/category/add'] = 'userctrl/add_category';

$route['userctrl/category/list'] = 'userctrl/list_category';
$route['Userctrl/category/list'] = 'userctrl/list_category';

$route['Userctrl/category/delete/(:any)'] = 'userctrl/delete_category/$1';
$route['userctrl/category/delete/(:any)'] = 'userctrl/delete_category/$1';
$route['pages/(:any)'] = 'home/pages/$1';
$route['newsdetail/(:any)'] = 'home/news/$1';
$route['products'] = 'home/products';
$route['testimonial'] = 'home/alltestimonials';
$route['search'] = 'home/products';
$route['news'] = 'home/allnews';
$route['contact'] = 'home/contactus';
$route['category/(:any)'] = 'home/products/$1';

