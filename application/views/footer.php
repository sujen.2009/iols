   
    <footer>
        <div class="row">
            <div class="col-sm-12">
                <div>
                    Copyright @ Inside out Living
                </div>
            </div>
        </div>     
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url(); ?>themes/js/bootstrap.min.js"></script>
        <!-- Morris Charts JavaScript -->
       
        <script type="text/JavaScript">
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.split(search).join(replacement);
            };

            $(document).ready(function(){
                $(".latestProduct .item .productList").flip({
                  trigger: 'hover'
                });

                    $('.addtocart,.detail_cart').on('click',function(){
                        var productid=$(this).attr('data-id');
                        $.get('<?php echo base_url(); ?>home/getProductDetail/'+productid,function(data){
                            console.log(data);
                            var obj=JSON.parse(data);
                            $('.imgholder img').attr('src',"http://localhost/ioms_git/themes/uploads/product/product_"+obj.product_id+"/"+obj.images.image_name);
                            $('.productname').html(obj.product_title);
                            $('.productname').attr('data-productid',obj.product_id);
                            var product_price=obj.product_price.replaceAll('"','').replaceAll('[','').replaceAll(']','');
                            var product_unit=obj.product_unit.replaceAll('"','').replaceAll('[','').replaceAll(']','');
                            product_unit=product_unit.split(",");
                            product_price=product_price.split(",");
                            $('.selectpicker').html("");
                            product_unit.forEach(function(x,i){
                                $('.selectpicker').append("<option data-price='"+product_price[i]+"'>"+x+"</option>");
                            })
                            $('#cartModal').modal('show');

                            $('.selectpicker').selectpicker('refresh');
                            computePrice();
                        })
                    })

                $('.selectedQty').change(function(){
                    computePrice();
                })

                 $('#cartModal .selectpicker').on('changed.bs.select ', function (e) {
                    computePrice();
                });

            })

            var computePrice=function(){
                  selectedPrice= parseFloat($('.selectpicker').find(":selected").attr('data-price'));
                  unit= $('.selectpicker').find(":selected").val();
                  var qty=parseFloat($('.selectedQty').val());
                  $('.rate').attr('data-rate',(selectedPrice))
                  $('.totalprice').attr('data-price',(selectedPrice*qty))
                  $('.rate').text("$"+(selectedPrice)+" / "+unit)
                  $('.totalprice').text("$"+(selectedPrice*qty))
                  
                  // debugger;
            }

        </script>
    </footer>

    </div>
</body>

</html>
<!-- Modal -->
<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title productname" data-productid='0' id="myModalLabel">Add Product to cart</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6 imgholder">
            <img src="http://localhost/ioms_git/themes/uploads/product/product_4/1473488383_thumb_blue_limestone_honed_2_4_2.jpg">
          </div>
          <div class="col-md-6">
              <div class="row orderholder">
                <div class="col-md-12">
                    <div class="order-holder"> 
                      <input class="selectedQty" value="0" />
                      <select class="selectpicker" >
                        <option>Select</option>
                      </select>
                    </div>

                </div>
                <div class="col-md-12">

                    <div class="order-rate-holder"> 
                      Rate : <span class='rate' data-rate='0'>$0</span>
                    </div>

                    <div class="order-price-holder"> 
                      Price : <span class="totalprice" data-price='0'>$0</span>
                    </div>

                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btncancel" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btnSave">Add to cart</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    
$(function(){
  var width=$(window).width();
                  
  $('.btnSave').on('click',function(){

      var saveJSON={};
      saveJSON.rate=parseFloat($('.rate').attr('data-rate'));
      saveJSON.totalprice=parseFloat($('.totalprice').attr('data-price'));
      saveJSON.product_id=$('.productname').attr('data-productid');
      saveJSON.unit=$('.selectpicker').val();
      saveJSON.quantity=$('.selectedQty').val();
      console.log(saveJSON);
      $.post('<?php echo base_url() ?>home/savetocart',(  saveJSON),function(){
          notif({
                          type: "error",
                          msg: "Product has beed added to cart. Click here to view all products in cart.",
                          position: "center",
                          width: width,
                          height: 60,
                           bgcolor: "rgb(113, 181, 108)",
                          autohide: true
                        });
                      $('#cartModal').modal('hide');
        $('.checkout').show();
      })


                    
  })
})

$(document).on('click',function(){
    $('.testimonial-div').on('mouseenter',function(){
        scroll($(this));
    })
    $('.testimonial-div').on('mouseleave',function(){
        $('.testimonial-div div').stop();
    })
})

var scroll=function(e){
    var height=parseInt(e.find('div').height())-120;
    e.find('div').animate({
         top:"-"+height+"px"
        },(height*50))
}

</script>
<?php
if($this->session->userdata('cart')){
?>
  <div class="checkout">
    <a href="<?php echo base_url(); ?>home/checkout"><img src='<?php echo base_url(); ?>themes/images/checkout.jpg'></a>
  </div>
<?php 
}else{
  ?>
    <div class="checkout" style="display:none">
    <a href="<?php echo base_url(); ?>home/checkout"><img src='<?php echo base_url(); ?>themes/images/checkout.jpg'></a>
  </div>
  <?php
}
?>