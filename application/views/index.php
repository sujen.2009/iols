





    <div id="pageWrapper">

       <div id="page-wrapper">  
            <div class="row">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <?php
                        foreach($sliders as $key=>$slider){

                            echo ' <li data-target="#myCarousel" data-slide-to="'.$key.'" class="active"></li>';
                           
                        }
                    ?>
                   
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <?php
                        foreach($sliders as $key=>$slider){
                            $class="";
                            if($key==0)
                                $class="active";
                            echo "<div class='item ".$class."'>";
                            echo "<img title='".$slider['slider_caption']."' src='".base_url()."themes/uploads/banner/".$slider['image_name']."'>";
                            echo "</div>";
                        }
                    ?>
                    
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>



            <div class="container">   
                <div class="row">
                    <div class="col-md-8">
                        <h3><?php
                            echo $welcometext['page_title'];
                        ?></h3>
                        <?php
                            echo $welcometext['page_short_description'];
                        ?>
                        <a href='<?php echo base_url(); ?>pages/<?php echo urlencode($welcometext["page_title"]) ?>'>Read more</a>
                    </div>
                    <div class="col-md-4">
                       
                        <div class="row" style="margin-top:20px;">
                            
                                <div class="col-xs-12">
                                   
                                      <?php
                                        $this->load->view('latestnews');
                                      ?>
                                </div>
                        </div>
                       
                    </div>
                </div>
            </div>
         <!--    <div class="container">   
                <div class="row">
                    <div class="col-sm-10">
                        <h3>Category</h3>
                    </div>
                    <div class="col-sm-2 viewall">
                        <h3>View All <span class="fa fa-long-arrow-right"></span></h3>
                    </div>
                </div>
                <div class="row productCatalog">

                        <div class="col-sm-3 item">
                            
                            <div class="innerItem">
                                <div class="row itembase">
                                        <div class="col-md-12">

                                        </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 item">
                            <div class="innerItem">
                                <div class="row itembase">
                                        <div class="col-md-12">

                                        </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 item">


                            <div class="innerItem">                            
                                <div class="row itembase">
                                        <div class="col-md-12">

                                        </div>

                                </div>
                            </div>
                            
                        </div>   
                        <div class="col-sm-3 item">

                            <div class="itemGrpCol">
                                <div class="row itemgrp">
                                        <div class="col-md-12">

                                            <div class="row catgrp">
                                                <div class="col-md-6">
                                                    <div></div>
                                                </div>
                                                <div class="col-md-6">                                                
                                                    <div></div>
                                                </div>
                                            </div>
                                            <div class="row  catgrp">
                                                <div class="col-md-6">                                                
                                                    <div></div>
                                                </div>
                                                <div class="col-md-6">                                                 
                                                    <div></div>
                                                </div>
                                            </div>

                                        </div>

                                </div>
                                
                                
                            </div>
                        </div>   

                </div>
            </div>
 -->

             <div class="container">   
                <div class="row">
                    <div class="col-sm-10">
                        <h3>Lastest Products</h3>
                    </div>
                    <div class="col-sm-2 viewall">
                        <h3><a href="<?php echo base_url() ?>products">View All <span class="fa fa-long-arrow-right"></span></a></h3>
                    </div>
                </div>
                <div class="row latestProduct">
                <?php 
                    foreach($products as $product){
                        $productid=$product['product_id'];
                        echo '
                                <div class="col-sm-2 item">
                                    <div class="productList"> 
                                        <div class="front"> 
                                            <img class="slides" src="'.base_url().'themes/uploads/product/product_'.$product['product_id'].'/'.$product['slide_images']['image_name'].'"">
                                          </div> 
                                          <div class="back">
                                            <div class="product_detail">
                                                <span>'.$product["product_title"].'</span>
                                                <div class="pricesection">Price : <span>$'.(( gettype(json_decode($product["product_price"]))=="array") ? json_decode($product["product_price"])[0] : $product["product_price"]) .' per '.((gettype(json_decode($product["product_price"]))=="array") ? json_decode($product["product_unit"])[0] : $product["product_unit"]) . '</span></div>
                                                <div class="linksection">
                                                    <a target="_blank" href="'.base_url().'product_detail/'.$product["product_title"].'">View Detail</a>
                                                    <a href="javascript:" data-id="'.$productid.'" class="addtocart">Add to cart</a>
                                                </div>
                                            </div> 

                                          </div> 
                                    </div> 
                                </div>
                        ';
                    }
                ?>
                   
                </div>
            </div>

             <div class="container testimonials">   
                <div class="row">
                    <div class="col-sm-10">
                        <h3>Reviews</h3>
                    </div>
                    <div class="col-sm-2 viewall">
                        <h3><a href='<?php echo base_url(); ?>testimonial'>View All <span class="fa fa-long-arrow-right"></span></a></h3>
                    </div>
                </div>
                <div class="row testimonialList">
                    <?php
                        foreach($testimonials as $key=>$testimonial){
                            if($key!=0 && $key%2==0){
                                echo ' </div><div class="row testimonialList">';
                            }
                            echo '
                                    <div class="col-sm-6 item testimonial">
                                            <div class="row" style="margin:0px;">
                                                <div class="col-sm-4">
                                                    <img src="'.base_url().'themes/images/2.jpg" style="width:100%" alt="" />
                                                </div>
                                                <div class="col-sm-8">
                                                     <div class=""><div>'.substr(strip_tags($testimonial["page_description"]),0,270).' ...<a href="'.base_url().'testimonial#'.$testimonial["page_title"].'">Read more</a></div></div>
                                                    <h4>-'.$testimonial["page_title"].'"</h3>
                                                </div>
                                            </div>
                                    </div>
                            ';
                        }
                    ?>
                   
                </div>
               
            </div>

             <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                 <form method="post" action="<?php echo base_url() ?>home/sendContactMail" class="form-horizontal">
                    <fieldset>
                        <legend class="text-center header">Contact us</legend>

                     <div class="row" style="margin:0;">
                    <div class="col-md-6" style="padding-right:40px;">
                        <div class="form-group">
                            <label for="name">
                                Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" email="email" placeholder="Enter email" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subject</label>
                            <select id="subject" name="subject" class="form-control" required="required">
                                <option value="na" selected="">Choose One:</option>
                                <option value="service">General Customer Service</option>
                                <option value="suggestions">Suggestions</option>
                                <option value="product">Product Support</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Send Message</button>
                    </div>
                </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>   

        </div>
    </div>
  <script type="text/javascript">
  $(function(){
      $("#demo3").bootstrapNews({
            newsPerPage: 3,
            autoplay: false,
            
            onToDo: function () {
                //console.log(this);
            }
        });
  })
  </script>