<div class="row">
<div class="container">
<div class="row shoppingCart ">
  <div class="col-md-12">
    <h3>My Shopping Cart</h3>
    <?php
      if(sizeof($products)>0){

      $totalprice=0;
      foreach($products as $product){

          echo "<div class='row productPlace'>";
            echo "<div class='col-sm-1'>";
            echo '<img class="slides productImg" src="'.base_url().'themes/uploads/product/product_'.$product['product_id'].'/'.$product['slide_images']['image_name'].'"">';
            echo "</div>";
            echo "<div class='col-sm-4'>";
              echo $product['product_title'];
              // print_r(json_decode($product['product_price']));
              $rate=json_decode($product['product_price']);
              // $rate=explode(",",$product['product_price']);
              $units=json_decode($product['product_unit']);
              $key = array_search($product['selectedUnit'], $units);
              echo '<br /><a target="_blank" href="'.base_url().'product_detail/'.$product["product_title"].'">View Detail</a>';
              // print_r($product);
            echo "</div>";
           
            echo "<div class='col-sm-2'>";
              echo $rate[$key]." per ".$product['selectedUnit'];
            echo "</div>";
             echo "<div class='col-sm-2'>";
              echo $product['quantity']." ".$product['selectedUnit'];
            echo "</div>";
             echo "<div class='col-sm-2'>";
              echo "$".($product['quantity']*$rate[$key])." only";
            echo "</div>";
             echo "<div class='col-sm-1'>";
              echo "<a href='".base_url()."home/removeFromCart/".$product['cart_id']."'>X</a>";
            echo "</div>";

          echo "</div>";

          $totalprice+=$product['quantity']*$rate[$key];
      }  

    ?>
  

    <div class="row totalprice">
      <div class="col-sm-9" style="text-align:right;"> 
          Total:
      </div>
      <div class="col-sm-3">
          $<?php echo $totalprice; ?> only
      </div>

    </div>
  <form class="shippingForm">
  <div class="row">
    <div class="col-sm-12">
      <h3>Shipping Information</h3>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-sm-2 shippingHeader">
      <label for="country">Country :</label>
    </div>
    <div class="col-sm-3">
      <select class="selectpicker country" name="country">
          <option>Australia</option>
      </select>
    </div>
  </div>
  <div class="row">
    
    <div class="col-sm-3 shippingHeader">
      <label for="state">State :</label>
    </div>
    <div class="col-sm-3">
      <select required class="selectpicker state" name="state">
        <option value="">Please select region, state or province</option>
        <option value="Australia Capital Territory">Australia Capital Territory</option>
        <option value="New South Wales">New South Wales</option>
        <option value="Northern Territory">Northern Territory</option>
        <option value="Queensland">Queensland</option>
        <option value="South Australia">South Australia</option>
        <option value="Tasmania">Tasmania</option>
        <option value="Victoria">Victoria</option>
        <option value="Western Australia">Western Australia</option>
      </select>
    </div>
  </div>
  <div class="row">
    
    <div class="col-sm-3 shippingHeader">
      <label for="fullname">Full name :</label>
    </div>
    <div class="col-sm-3 ">
      <input required class="form-control" type="text" name="fullname"  id="fullname" />
    </div>
  </div>
  <div class="row">
    
    <div class="col-sm-3 shippingHeader">
      <label for="contact">Contact Number :</label>
    </div>
    <div class="col-sm-3 ">
      <input required  class="form-control" type="number" name="contact"  id="contact" />
    </div>
  </div>
  <div class="row">
    
    <div class="col-sm-3 shippingHeader">
      <label for="shipping_address">Shipping address :</label>
    </div>
    <div class="col-sm-3 ">
      <input type="hidden" name="total_price" value="<?php echo $totalprice; ?>" />
      <input required class="form-control" type="text" name="shipping_address"  id="shipping_address" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3 shippingHeader">
      <label for="email">Email address :</label>
    </div>
    <div class="col-sm-3 ">
      <input required class="form-control" type="email" name="email"  id="email" />
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3 shippingHeader">
      <label for="email">Payment Method :</label>
    </div>
    <div class="col-sm-3 ">
      <select required class="selectpicker" name='paymentMethod'>
          <option value="">Choose payment Method</option>
          <option value="1">On delivery</option>
      </select>
    </div>
  </div>
  <div  class="row">
      <div class="col-sm-12">
          <input name='verify'  type="checkbox" id="verify"> <label for="verify">I hereby verify that all the information provided are correct.</label>
      </div>
  </div>
  <div class="row">
    <div class="col-md-6">
        <div class="btn-wrap">
            <div class="place_order"  style="float:right; margin-top:0px;">
                <a href="javascript:" class="pace_order">Place an order</a>
            </div>
        </div>
    </div>
  </div>
  </form>
  <?php
  }else{
    echo "There is no item in your cart. Click <a href='".base_url()."'>here</a> to start Shopping!!";
  }
  ?>
</div>
</div>
</div>
</div>

<style type="text/css">
  .shoppingCart img.productImg{
      max-width: 100%;
      max-height: 100%;

  }

  .shoppingCart .productPlace{
     padding:10px;
  }
  .shoppingCart .productPlace:hover{
    background: #ccc;

  }

  .shoppingCart .totalprice{
    background: #ccc;
    padding:10px;
    font-weight: bolder;
  }

  .shoppingCart .row{
    margin-bottom: 20px;
  }

  .shippingHeader{
    font-size: 18px;
    text-align: right;
  }
  .shippingHeader label{
    font-weight: normal;
  }

</style>

<script>
var oncevalidated=false;
$(function(){


  $('.selectpicker').on('changed.bs.select ', function (e) {
    if(oncevalidated==true)
      $('.shippingForm').validate().form();
  });

  $('.pace_order').on('click',function(){
    var validatetest=$('.shippingForm').validate().form();
    oncevalidated=true;
    if(validatetest){
      if($('#verify').prop('checked')==false){
        alert("You need to verify the details!!! ");
        return false;
      }
      var con=confirm('Are you sure you want to place an order?');
        if(con){
            $.post('<?php echo base_url(); ?>home/orderPlacement',$('.shippingForm').serialize(),function(data){
                console.log(data);
                if(data=="Success"){
                  alert("Your order has been placed successfully. If you have any query please contact us through contact page.");
                  window.location="<?php echo base_url(); ?>";
                }
            })
        }
    }    
  })
})
</script>