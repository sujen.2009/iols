





    <div id="pageWrapper">

       <div id="page-wrapper">  
             <div class="container">   
                <div class="row">
                    <div class="col-md-3">
                            <h3>Categories</h3>
                            <div id="categoryTree">
                              <ul>
                                <?php
                                    foreach($categories as $cat){
                                        $cattitle=urlencode($cat['category_title']);
                                        echo "<li><a href='".base_url()."category/".$cattitle."'>".$cat['category_title']."</a>";
                                        if(sizeof($cat['child'])>0)    
                                            childCats($cat['child']);
                                        echo "</li>";
                                    }
                                ?>  
                              </ul>
                            </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row latestProduct">
                            <h3>
                                <?php
                                    if($searchkey!=""){
                                        echo "Search results for <i><u>".$searchkey."</u></i>";
                                    }else if($selectedCategory==""){
                                        echo "Products";
                                    }else{
                                        echo $selectedCategory;
                                    }
                                ?>
                            </h3>
                            <?php 
                            if(sizeof($products)==0){
                                echo "No products found";
                            }
                                foreach($products as $product){
                                    $productid=$product['product_id'];
                                    echo '
                                            <div class="col-sm-3 item">
                                                <div class="productList"> 
                                                    <div class="front"> 
                                                        <img class="slides" src="'.base_url().'themes/uploads/product/product_'.$product['product_id'].'/'.$product['slide_images']['image_name'].'"">
                                                      </div> 
                                                      <div class="back">
                                                        <div class="product_detail">
                                                            <span>'.$product["product_title"].'</span>
                                                            <div class="pricesection">Price : <span>$'.(( gettype(json_decode($product["product_price"]))=="array") ? json_decode($product["product_price"])[0] : $product["product_price"]) .' per '.((gettype(json_decode($product["product_price"]))=="array") ? json_decode($product["product_unit"])[0] : $product["product_unit"]) . '</span></div>
                                                            <div class="linksection">
                                                                <a target="_blank" href="'.base_url().'product_detail/'.$product["product_title"].'">View Detail</a>
                                                                <a href="javascript:" data-id="'.$productid.'" class="addtocart">Add to cart</a>
                                                            </div>
                                                        </div> 

                                                      </div> 
                                                </div> 
                                            </div>
                                    ';
                                }
                            ?>
                               
                            </div>

                    </div>

                </div>
                
            </div>



        </div>
    </div>
  <script type="text/javascript">
  $(function(){
    $('#categoryTree').jstree({

     "core": {
        "themes":{
            "icons":false
        }
    }
    }).bind("select_node.jstree", function (e, data) {
     var href = data.node.a_attr.href;
     document.location.href = href;
});
      $("#demo3").bootstrapNews({
            newsPerPage: 3,
            autoplay: false,
            
            onToDo: function () {
                //console.log(this);
            }
        });
  })
  </script>

  <?php
    function childCats($childs){
        echo "<ul>";
        foreach($childs as $cat){
         $cattitle=urlencode($cat['category_title']);
            echo "<li><a href='".base_url()."category/".$cattitle."'>".$cat['category_title']."</a>";
            if(sizeof($cat['child'])>0)    
                childCats($cat['child']);
            echo "</li>";
        }
        echo "</ul>";
    }
  ?>