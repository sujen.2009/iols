



    <div id="pageWrapper">

       <div id="page-wrapper">  
            <div class="container">   
                <div class="row">
                    <?php
		$imagelocation=base_url()."themes/images/no_product_img.jpg";

                           		if(isset($image["image_name"])){

                           			$imagelocation=base_url().'themes/uploads/product/product_'.$product_detail["product_id"].'/'.$image["image_name"];

                           		}

?>

	<section class="inner-page product-detail wow fadeInLeft" data-wow-delay="1s">
	<div class="row">
	<div class="col-md-8">
			<div class="wrapper">
				<div class="primary-content">
				<?php echo $this->session->flashdata('message_success'); ?>
					<h1 class="main-title"><?php echo $product_detail['product_title'] ?></h1>
					<div class="inner-page-content">
						<div class="product-detail-wrap clearfix">
							<figure class="magnify">
								<div class="large"></div>
								<!-- <img class="small" src="<?php echo base_url(); ?>themes/images/product-detail-img.jpg" alt="section-img"> -->
							
								<img src="<?php echo $imagelocation ?>" alt="" class="small"> 

							</figure>
							<div class="product-detail-content">
								<table>
									<tr>
										<th>Product name</th>
										<td class="product-detail-seperater">:</td>
										<td><?php echo $product_detail['product_title'] ?></td>
									</tr>
									<?php 
										// print_r($product_detail);
									// foreach($etiquates as $etiquate){
									// 	if($etiquate["etiquate_desc"]!=""){
									// 		echo '
									// 		<tr>
									// 			<th>'.$etiquate["etiquate_title"].'</th>
									// 			<td class="product-detail-seperater">:</td>
									// 			<td>'.$etiquate["etiquate_desc"].'</td>
									// 		</tr>
									// 	';
											
									// 	}
										
									// }

									?>
									<tr>
										<th>Price</th>
										<td class="product-detail-seperater">:</td>
										<td><?php echo json_decode($product_detail['product_price'])[0] ." per ".json_decode($product_detail['product_unit'])[0] ?></td>
									</tr>
									<tr>
									<td>
										<div class="btn-wrap">
								<div class="detail_cart"  data-id="<?php echo $product_detail['product_id']; ?>">
										<a href="javascript:" >Add to Cart</a>
								</div>
						</div>
									</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="product-detail-desc" style="padding-left:20px;">
							<!-- <h2>Other Description</h2> -->
							<?php echo $product_detail['product_brief_info']; ?>
						</div>
						
					</div>

				</div>
				
			</div>

		</div>
		<div class="col-md-4">
			<div class="row">
					<div class="col-md-12">
							<h3>Latest News</h3>
					</div>
					<div class="col-md-12">	

					</div
								</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div id="disqus_thread"></div>
<script>

/**
 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */
/*
var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = '//inside-out-living-solution.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
		</div>

	</div>
	</section> <!------- -- INNER PAGE -------- -->

  
            </div>
           

        </div>
    </div>
     <div class="container">   
                <div class="row">
                    <div class="col-sm-10" style="padding:0;">
                    <div class="product-detail-desc">
						 <h3>Related Products</h3>
					</div>
                       
                    </div>
                    <div class="col-sm-2 viewall">
                        <h3>View All <span class="fa fa-long-arrow-right"></span></h3>
                    </div>
                </div>
                <div class="row latestProduct">
                <?php 
                    foreach($products as $product){
                         $productid=$product['product_id'];
                        echo '
                                <div class="col-sm-2 item">
                                    <div class="productList"> 
                                        <div class="front"> 
                                            <img class="slides" src="'.base_url().'themes/uploads/product/product_'.$product['product_id'].'/'.$product['slide_images']['image_name'].'"">
                                          </div> 
                                          <div class="back">
                                            <div class="product_detail">
                                                <span>'.$product["product_title"].'</span>
                                                <div class="pricesection">Price : <span>$'.(( gettype(json_decode($product["product_price"]))=="array") ? json_decode($product["product_price"])[0] : $product["product_price"]) .' '.((gettype(json_decode($product["product_price"]))=="array") ? json_decode($product["product_unit"])[0] : $product["product_unit"]) . '</span></div>
                                                <div class="linksection">
                                                    <a target="_blank" href="'.base_url().'product_detail/'.$product["product_title"].'">View Detail</a>
                                                    <a href="javascript:" data-id="'.$productid.'" class="addtocart">Add to cart</a>
                                                </div>
                                            </div> 

                                          </div> 
                                    </div> 
                                </div>
                        ';
                    }
                ?>
                   
                </div>
            </div>