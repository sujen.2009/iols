<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo base_url(); ?>themes/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>themes/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>themes/css/adminlte.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>themes/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">
                <img src="<?php echo base_url(); ?>themes/images/logo.jpg" style='width:30%' class=''>
                <span>Admin Login</span>
            </div>
            <form action="<?php echo base_url(); ?>login/login_process" method="post">
            
                <div class="body bg-gray">
                <?php
                echo $this->session->flashdata('login_error');
            ?>
                    <div class="form-group">
                        <input type="text"  name="userid" class="form-control" placeholder="User Name"/>
                        <?php
                                    echo form_error('userid');
                        ?>
                    </div>
                    <div class="form-group">
                        <input type="password"  name="password" class="form-control" placeholder="Password"/>
                       
                        <?php
                                    echo form_error('password');
                        ?>
                    </div>          
                    <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">LogIn</button>  
                    
                </div>
            </form>
        </div>

        <script src="<?php echo base_url(); ?>themes/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>themes/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>
