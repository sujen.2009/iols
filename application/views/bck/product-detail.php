<?php
		$imagelocation=base_url()."themes/images/no_product_img.jpg";

                           		if(isset($image["image_name"])){

                           			$imagelocation=base_url().'themes/uploads/product/product_'.$product_detail["product_id"].'/'.$image["image_name"];

                           		}

?>

	<section class="inner-page product-detail wow fadeInLeft" data-wow-delay="1s">
		<div class="wrapper">
			<div class="primary-content">
			<?php echo $this->session->flashdata('message_success'); ?>
				<h1 class="main-title"><?php echo $product_detail['product_title'] ?></h1>
				<div class="inner-page-content">
					<div class="product-detail-wrap clearfix">
						<figure class="magnify">
							<div class="large"></div>
							<!-- <img class="small" src="<?php echo base_url(); ?>themes/images/product-detail-img.jpg" alt="section-img"> -->
						
							<img src="<?php echo $imagelocation ?>" alt="" class="small"> 

						</figure>
						<div class="product-detail-content">

							<table>
								<tr>
									<th>Product name</th>
									<td class="product-detail-seperater">:</td>
									<td><?php echo $product_detail['product_title'] ?></td>
								</tr>
								<?php 
									// print_r($product_detail);
								foreach($etiquates as $etiquate){
									if($etiquate["etiquate_desc"]!=""){
										echo '
										<tr>
											<th>'.$etiquate["etiquate_title"].'</th>
											<td class="product-detail-seperater">:</td>
											<td>'.$etiquate["etiquate_desc"].'</td>
										</tr>
									';
										
									}
									
								}

								?>
								<tr>
									<th>Price</th>
									<td class="product-detail-seperater">:</td>
									<td><?php echo $product_detail['product_price'] ?></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="product-detail-desc">
						<h2>Other Description</h2>
						<?php echo $product_detail['product_desc']; ?>
					</div>
					<div class="btn-wrap">
							<a href="<?php echo base_url(); ?>enquiry/<?php echo $product_detail['product_title']; ?>">Request a quote</a>
				</div>
				</div>
			</div>
			
		</div>
	</section> <!------- -- INNER PAGE -------- -->
