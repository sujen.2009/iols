
<div class="pageContainer container-fluid">
    


    <header>
                <div class="row logoSection">
                    <div class="col-lg-12">

                        <div class="container">
                            <img src="<?php echo base_url(); ?>themes/images/logo.jpg">

                        </div>

                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-12 menuSection">   
                        
                        <nav class="navbar navbar-default">
                            <div class="container">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                  <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#">About US</a></li>
                                    <li><a href="#">Products</a></li>
                                    <li><a href="#">News</a></li>
                                    <li><a href="#">Gallery</a></li>
                                    <li><a href="#">Contact us</a></li>
                                  </ul>
                                <!--   <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Link</a></li>
                                  </ul> -->
                                </div>
                            </div>
                        </nav>
                    </div>    
                </div>    
    </header>







    <div id="pageWrapper">

       <div id="page-wrapper">  
            <div class="container">   
                <div class="row">
                    <div class="inner_body">
                        <div class="inner_title">
                            Product: <?php echo $product_detail['product_title'] ?>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="<?php echo base_url(); ?>themes/uploads/product/product_<?php echo $product_detail['product_id'] ?>/<?php echo $image['image_name'] ?>" alt="" class="img-responsive product-detail-img"> 
                                </div>
                                <div class="col-sm-9">
                                    <div class="well">
                                        <span class="product-detail-title">Product Title:</span> <span class="product-detail-text"><?php echo $product_detail['product_title'] ?></span>
                                        <span class="product-detail-title">Category:</span> <span class="product-detail-text"><?php echo $product_detail['category_title'] ?></span>
                                        <span class="product-detail-title">Price:</span> <span class="product-detail-text"><?php echo $product_detail['product_price'] ?></span>
                                        <span class="product-detail-title">Total views:</span> <span class="product-detail-text">100 time(s)</span>    
                                        <span class="product-detail-text"><a href="https://twitter.com/share" class="twitter-share-button">facebook like btn</a></span>
                                        <span class="product-detail-text"><a href="https://twitter.com/share" class="twitter-share-button">google plus</a></span>
                                    </div>
                                </div>
                                <div>
                                    <p class="product-detail-title">Description</p>
                                    <?php echo $product_detail['product_brief_info'] ?>
                                    <div class="btn btn-primary btn-group-sm pull-right margin-top"><a href="<?php echo base_url(); ?>enquiry/<?php echo $product_detail['product_title'] ?>">Enquiry</a></div>
                                    <div class="btn btn-default btn-group-sm pull-right margin-top" style="margin-right:10px;"><a href="<?php echo base_url(); ?>sharelink/<?php echo $product_detail['product_title'] ?>">Email to friend</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
  
            </div>
           

        </div>
    </div>
     
    <footer>
        
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url(); ?>themes/js/bootstrap.min.js"></script>
        <!-- Morris Charts JavaScript -->
       
        <script type="text/JavaScript">
            $(document).ready(function(){
                $(".latestProduct .item .productList").flip({
                  trigger: 'hover'
                });

            })
        </script>
    </footer>

    </div>
</body>

</html>
