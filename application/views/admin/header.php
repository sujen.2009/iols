<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IOLS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>themes/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>themes/js/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>themes/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url(); ?>themes/css/plugins/morris.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>themes/js/uploadify/uploadify.css" rel="stylesheet" type="text/css">
       
    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>themes/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>themes/js/jquery.js"></script>
      <script type="text/javascript" src="<?php echo base_url(); ?>themes/js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>

          <script src="<?php echo base_url() ?>themes/js/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>themes/js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url(); ?>themes/js/uploadify/jquery.uploadify.js"></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">IOLS</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Suren Maharjan <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url(); ?>userctrl/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="<?php echo base_url(); ?>userctrl"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#content"><i class="fa fa-building-o" aria-hidden="true"></i> Contents </a>
                        <ul id="content" class="collapse">
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/add_content">Add Content</a>
                            </li>
                            <li>
                                 <a href="<?php echo base_url(); ?>userctrl/list_page">List Contents</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#news"><i class="fa fa-newspaper-o" aria-hidden="true"></i> News Management </a>
                        <ul id="news" class="collapse">
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/news/add">Add News</a>
                            </li>
                            <li>
                                 <a href="<?php echo base_url(); ?>userctrl/news/list">List News</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#testimonial"><i class="fa fa-comment-o" aria-hidden="true"></i> Testimonial </a>
                        <ul id="testimonial" class="collapse">
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/add_testimonial">Add Testimonial</a>
                            </li>
                            <li>
                                 <a href="<?php echo base_url(); ?>userctrl/list_testimonial">List Testimonial</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#customer"><i class="fa fa-user" aria-hidden="true"></i> Customer Info </a>
                        <ul id="customer" class="collapse">
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/add_customer">Add Customer</a>
                            </li>
                            <li>
                                 <a href="<?php echo base_url(); ?>userctrl/list_customer">List Customer</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#product"><i class="fa fa-database" aria-hidden="true"></i> Products </a>
                        <ul id="product" class="collapse">
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/add_product">Add product</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/list_product">List products</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#category"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Categories </a>
                        <ul id="category" class="collapse">
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/add_category">Add category</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>userctrl/list_category">List category</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>userctrl/allOrders" data-toggle="collapse" data-target="#demo"><i class="fa fa-briefcase" aria-hidden="true"></i> Orders </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>userctrl/slider" data-toggle="collapse" data-target="#demo"><i class="fa fa-image" aria-hidden="true"></i> Sliding Images </a>
                    </li>
                     <li class="">
                            <a href="<?php echo base_url(); ?>userctrl/menu_mgmt" data-toggle="collapse" data-target="#demo"><i class="fa fa-reorder" aria-hidden="true"></i> Menu management </a>
                            
                        </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>




    <script>
            $(function(){
                var current=window.location.href;
                $('.side-nav li a').each(function(){
                    if($(this).attr('href')==current){

                        if($(this).parents('ul').hasClass('collapse')){
                            $(this).parents('ul').addClass('in');
                            $(this).parents('ul').parents('li').addClass('active');
                        }else{
                            $(this).parents('li').addClass('active');
                        }

                         $(this).addClass('active');
                    }
                })
            })

    </script>