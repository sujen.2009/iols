<script> 
    $(function() {
        var count=0;
        $('#file_upload').uploadify({
            'swf'      : '<?php echo base_url(); ?>themes/js/uploadify/uploadify.swf',
            'uploader' : '<?php echo base_url(); ?>themes/js/uploadify/uploadify.php',
            'fileExt'        : '*.jpg;*.gif;*.png',
            'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
            'multi'     : true,
            'onUploadComplete' : function(file) {
                $('.btnsubmit').show();
                $('#uploadedImages .imageclear').before('<li  style="width:200px;"><img style="width:100% !important;" src="<?php echo base_url(); ?>themes/temp/'+file.name+'"><input type="hidden" name="file_name_'+count+'"" value="'+file.name+'"><br /><input placeholder="Caption in english" name="caption_en_'+count+'" type="text" /><br /><input name="caption_np_'+count+'" placeholder="Caption in nepali" type="text" /></li>');
              //  alert('The file ' + file.name + ' finished processing.');
              $('#gallery_image_size').val(count);
              count++;
            }

            // Put your options here
        });
        $('.lang_select').hide();
    });
</script>





        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Update Image
                        </h1>
                       
                    </div>
                </div>


                 <div class="row galleryimagelist">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                  
                        <form method='post' action='<?php echo base_url(); ?>userctrl/updateSliderImage'>
                            <ul class='galleryimagelist'>
                            <?php
                                // foreach($gallery_images as $imgs){
                                    echo "

                                        <li>
                                            <img src='".base_url()."themes/temp/".$imgs['image_name']."'>
                                            <input type='hidden' name='slider_id' value='".$imgs['slider_id']."'>
                                            <br /><input placeholder='Caption in English' type='text' value='".$imgs['slider_caption']."' name='caption'>
                                            
                                            <button style='margin-top:10px;' class='btn btn-primary btn-lg' type='submit'>Update image</button>
                                        </li>
                                        
                                    ";

                                // }

                            ?>
                            <div class='clear'></div>
                            </ul>
                        </form>     
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>









      <style>
      #uploadedImages input{
        width:100%;
        margin-bottom:10px;
      }  
      #uploadedImages li,.galleryimagelist li{
        list-style-type:none;
        float:left;
        margin-left:50px;
        width:200px;


      }

      .galleryimagelist img{
        width:100%;
      }
      .galleryimagelist li{
        position:relative;
        width:40%
      }
      .galleryimagelist input{
        width:100%;
      }
      .galleryimagelist button.btnedit{
        position:absolute;
        bottom:10px; 
        right:20px;
      }  
      .galleryimagelist button.btndel{
        position:absolute;
        bottom:10px; 
        right:0px;
      }
      .galleryimagelist img{
            margin-bottom:10px;

      }
      </style>