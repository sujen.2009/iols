<script>
$(document).ready(function(){

    $('.lang_select').trigger('change');    
});

</script>
<style>
.lang_div{

    display:none;
}
</style>
          
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Content Header (product header) -->
                <section class="content-header" style='position:relative;'>
                    <h1>
                        Add products
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_product'); ?>'>
                                        <!-- text input -->
                                          <div class="form-group">
                                                <label>Product title</label>
                                                <input required type="text" placeholder="" name='product_title' class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Product description</label>
                                                <textarea placeholder="" name='product_brief_info'  class="form-control tinymce"></textarea>
                                            </div>
                                   
                                         
                                           
                                            <div class="form-group">
                                                <label>Product price</label>
                                                <div class="productPriceHolder">
                                                    <div class="row">
                                                        <div class="col-md-8">

                                                            <input class="form-control" type='number' placeholder="Product price" value="" name='product_price[]' style='width:100%;'>

                                                        </div>  
                                                        <div class="col-md-4" style="position:relative">
                                                            <input class="form-control" type='text' placeholder="Product unit" value="" name='product_unit[]' style='width:100%;'>
                                                        </div>  
                                                    </div>  
                                                </div>  
                                                <div class="row">
                                                    <div class="col-md-12"><br />
                                                        <button type="button" class="btn btn-default btnaddmore">Add more</button>  
                                                    </div>  
                                                </div>  
                                            </div>  

                                             <!-- textarea -->
                                            <div class="form-group">
                                                <label>Product category</label>
                                                <select class="form-control" style='width:100%' name='product_type'>
                                                <?php
                                                    foreach($categories as $cat){

                                                        echo "<option value='".$cat['category_id']."'>".$cat['category_title']."</option>";
                                                    }
                                                ?>
                                                </select>
                                            </div>
<!-- textarea -->
                                            <div class="form-group">
                                                <label>Meta Description</label>
                                                <textarea placeholder=""  name='meta_description' class="form-control"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>Meta Tags</label> <small>Separate Tags with comma ' , '</small> 
                                                <textarea placeholder="" name='meta_tags' class="form-control"></textarea>
                                            </div>
                                       

                                            <div class="form-group">
                                                <label for="exampleInputFile">Product Images <?php echo form_error('products_image'); ?></label>
                                                <input  type="file" name='image1' id="exampleInputFile">
                                                <input  type="file" name='image2' id="exampleInputFile">
                                                <input  type="file" name='image3' id="exampleInputFile">
                                                <input  type="file" name='image4' id="exampleInputFile">
                                            </div>
                                            
                                        <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <input type="file" name='thumbnailimage' id="exampleInputFile">
                                        </div>

                                      <div class="buttons container-fluid row">
                                                <div class="pull-right">
                                                <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                                <button class="btn btn-primary btn-lg" type="submit">Add products</button>
                                            </div>

                                    </form>
                                    </div>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>

                </section><!-- /.content -->
            </div><!-- /.right-side -->
            </div><!-- /.right-side -->
      <script type="text/javascript">
            $(function(){
                $('.btnaddmore').on('click',function(){
                    var html=$('.productPriceHolder div.row:first').html();
                    html="<div class='row' style='margin-top:5px;'>"+html+"</div>";
                    $('.productPriceHolder').append(html);
                    $('.productPriceHolder div.row:last  div:last').append("<a class='removeRow'><span style='position:absolute; right:20px; top:10px;' class='glyphicon glyphicon-trash'></span></a>")
                    $('.productPriceHolder div:last input').val('');
                    bindevent();
                })
            })

            var bindevent=function(){
                $('.removeRow').off('click').on('click',function(e){
                    e.preventDefault();
                    $(this).parent('div').parent('div').remove();
                })
            }
     </script>>