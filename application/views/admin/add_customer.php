
        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Customer Info
                        </h1>
                       
                    </div>
                </div>


                 <div class="row">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_customer'); ?>'>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <input required type="text" placeholder="" name='fullname' class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Address</label>
                                             <input required type="text" placeholder="" name='address' class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Contact no.</label>
                                             <input required type="text" placeholder="" name='contactno' class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Email address</label>
                                             <input required type="email" placeholder="" name='email_address' class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <input type="file" name='thumbnailimage' id="exampleInputFile">
                                        </div>

                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Add Customer</button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
