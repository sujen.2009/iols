<script>
$(document).ready(function(){
     <?php
        if($product_detail["product_type"]!=""){
     ?>   
    $('.product_category').val('<?php echo $product_detail["product_type"]; ?>');
    <?php
        }
    ?>
    $('.lang_select').trigger('change');    
});

</script>

<style>
.lang_div{

    display:none;
}
</style>
            <!-- Right side column. Contains the navbar and content of the product -->
               <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Content Header (product header) -->
                <section class="content-header" style='position:relative;'>
                    <h1>
                        Add products
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_product'); ?>'>
                                        <!-- text input -->
                                        <?php //echo print_r($language_avb); ?>
                                       
                                           <div class="form-group">
                                                <label>product Name</label>
                                                <input required type="text" value="<?php echo $product_detail['product_title'] ?>" placeholder="" name='product_title' class="form-control">
                                            </div>



                                            <div class="form-group">
                                                <label>product content</label>
                                                <textarea placeholder=""  name='product_brief_info'  class="form-control tinymce"><?php echo $product_detail['product_brief_info'] ?></textarea>
                                            </div>
                                           

                                            <div class="form-group">
                                                <label>Product price</label>
                                                <div class="productPriceHolder">
                                                    <?php

                                                        if($product_detail['product_price'] && gettype($product_detail['product_price'])=='array' && sizeof($product_detail['product_price'])!=0){
                                                            foreach($product_detail['product_price'] as $key=>$price){
                                                                ?>
                                                                     <div class="row" style='margin-top:5px;'>
                                                                            <div class="col-md-8">
                                                                                <input class="form-control" type='number' placeholder="Product price" value="<?php echo $price ?>" name='product_price[]' style='width:100%;'>
                                                                            </div>  
                                                                            <div class="col-md-4" style="position:relative">
                                                                                <input class="form-control" type='text' placeholder="Product unit" value="<?php echo $product_detail['product_unit'][$key] ?>" name='product_unit[]' style='width:100%;'>
                                                                                <?php
                                                                                    if($key!=0){
                                                                                        echo "<a class='removeRow'><span style='position:absolute; right:20px; top:10px;' class='glyphicon glyphicon-trash'></span></a>";
                                                                                    }
                                                                                ?>
                                                                            </div>  
                                                                        </div>  
                                                                <?php
                                                            }
                                                        }else{
                                                             ?>                   
                                                              
                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <input class="form-control" type='number' placeholder="Product price" value="" name='product_price[]' style='width:100%;'>
                                                                    </div>  
                                                                    <div class="col-md-4" style="position:relative">
                                                                        <input class="form-control" type='text' placeholder="Product unit" value="" name='product_unit[]' style='width:100%;'>
                                                                    </div>  
                                                                </div>  
                                                           
                                                                <?php

                                                        }
                                                    ?>
                                                   
                                                </div>  
                                                <div class="row">
                                                    <div class="col-md-12"><br />
                                                        <button type="button" class="btn btn-default btnaddmore">Add more</button>  
                                                    </div>  
                                                </div>  
                                            </div>  
                                          
                                            
                                              <div class="form-group">
                                                <label>Product category</label>
                                                <select class="form-control" style='width:100%' name='product_type' class='product_category'>
                                                <?php
                                                    foreach($categories as $cat){

                                                        echo "<option value='".$cat['category_id']."'>".$cat['category_title']."</option>";
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <!-- textarea -->
                                            <div class="form-group">
                                                <label>Meta Description</label>
                                                 <input type='hidden' name='product_id', value='<?php echo $product_detail["product_id"]; ?>'>
                                                <textarea placeholder=""  name='meta_description' class="form-control"><?php echo $product_detail['meta_description']; ?></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>Meta Tags</label> <small>Separate Tags with comma ' , '</small> 
                                                <textarea placeholder="" name='meta_tags' class="form-control"><?php echo $product_detail['meta_tags']; ?></textarea>
                                            </div>
                                         
 <div class="form-group">               <div id='image_process' class="form-group">
                                         <!-- 
                                            <br /><span class='note'> [ Image should be less than 3 MB ]</span>
                                         -->
                                            <label for="exampleInputFile">Product Images</label>
                                            <?php
                                                $total=4-sizeof($slide_images);
                                                for($i=1;$i<=$total;$i++){

                                                    echo '
                                                        <input  type="file" name="image'.$i.'" id="exampleInputFile">
                                                    ';

                                                }
                                            ?>
                                           
                                        </div>
                                        <?php
                                                if(sizeof($slide_images)!=0){
                                            ?>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Uploaded images :</label>
                                            <table class='sliding_images'>
                                                <tr>
                                                    <?php
                                                        foreach($slide_images as $img){
                                                                echo "<td>
                                                                            <div class='outer-slide'>
                                                                            <img class='slides' src='".base_url()."themes/uploads/product/product_".$product_detail['product_id']."/".$img['image_name']."'>
                                                                                <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$img['image_id']."/product'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                            </div>
                                                                        </td>";
                                                        }
                                                    ?>
                                                </tr>
                                            </table>
                                        </div>
                                        <?php
                                               }
                                            ?>
                                      
                                        <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <?php
                                                if(sizeof($thumbnail)==0){
                                            ?>
                                                <input type="file" name='thumb_image' id="exampleInputFile">
                                            <?php
                                            }else{
                                                ?>
                                                <table class='sliding_images'>
                                                    <tr>
                                                        <?php
                                                            
                                                            echo "<td>
                                                                        <div class='outer-slide'>
                                                                        <img class='slides' src='".base_url()."themes/uploads/product/product_".$product_detail['product_id']."/".$thumbnail['image_name']."'>
                                                                            <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$thumbnail['image_id']."/page'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                        </div>
                                                                    </td>";
                                                           
                                                        ?>
                                                    </tr>
                                                </table>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                      <div class="buttons container-fluid row">
                                                <div class="pull-right">
                                                <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                                <button class="btn btn-primary btn-lg" type="submit">Update product</button>
                                            </div>

                                    </form>
                                    </div>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>

                </section><!-- /.content -->
            </div><!-- /.right-side -->
            </div><!-- /.right-side -->
     <script type="text/javascript">
            $(function(){
                $('.btnaddmore').on('click',function(){
                    var html=$('.productPriceHolder div.row:first').html();
                    html="<div class='row' style='margin-top:5px;'>"+html+"</div>";
                    $('.productPriceHolder').append(html);
                    $('.productPriceHolder div.row:last  div:last').append("<a class='removeRow'><span style='position:absolute; right:20px; top:10px;' class='glyphicon glyphicon-trash'></span></a>")
                    $('.productPriceHolder div.row:last input').val('');
                    bindevent();
                })
            })
            var bindevent=function(){
                $('.removeRow').off('click').on('click',function(e){
                    e.preventDefault();
                    $(this).parent('div').parent('div').remove();
                })
            }
            bindevent();
     </script>>