           <?php
            if(isset($category_detail['category_parent']) && $category_detail['category_parent']!=""){
           ?>
            <script>
                $(document).ready(function(){
                        $('.category_title').val(<?php echo $category_detail['category_parent'] ?>).attr('selected','selected');
                })
            </script>
            <?php
         }
            ?>
           
          
        <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Category
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-warning">
                                <div class="box-body">
                                    <form enctype="multipart/form-data" role="form" method='post' action='<?php echo base_url('userctrl/add_new_category'); ?>'>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Category Name</label>
                                            <input type='hidden' name='category_id' value='<?php echo $category_detail["category_id"] ?>'>
                                            <input required value='<?php echo set_value("category_title",$category_detail["category_title"]) ?>' type="text" name='category_title' placeholder="Category title" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Parent Category</label>
                                         
                                            <select name='category_parent' class="form-control category_title">
                                                <option SELECTED value='0'>NULL</option>
                                               <?php

                                               foreach($parent_categories as $cat){

                                                    echo "<option value='".$cat['category_id']."'>".$cat['category_title']."</option>";
                                               }
                                               ?>
                                            </select>
                                        </div>
<div class="form-group">               <div id='image_process' class="form-group">
                                         <!-- 
                                            <br /><span class='note'> [ Image should be less than 3 MB ]</span>
                                         -->
                                            <label for="exampleInputFile">Sliding Images <?php echo form_error('country_image'); ?></label>
                                            <?php
                                                $total=4-sizeof($slide_images);
                                                for($i=1;$i<=$total;$i++){

                                                    echo '
                                                        <input  type="file" name="image'.$i.'" id="exampleInputFile">
                                                    ';

                                                }
                                            ?>
                                           
                                        </div>
                                        <?php
                                                if(sizeof($slide_images)!=0){
                                            ?>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Uploaded images :</label>
                                            <table class='sliding_images'>
                                                <tr>
                                                    <?php
                                                        foreach($slide_images as $img){
                                                                echo "<td>
                                                                            <div class='outer-slide'>
                                                                            <img class='slides' src='".base_url()."themes/uploads/category/category_".$category_detail['category_id']."/".$img['image_name']."'>
                                                                                <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$img['image_id']."/category'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                            </div>
                                                                        </td>";
                                                        }
                                                    ?>
                                                </tr>
                                            </table>
                                        </div>
                                        <?php
                                               }
                                            ?>
                                               <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <?php
                                                if(sizeof($thumbnail)==0){
                                            ?>
                                                <input type="file" name='thumb_image' id="exampleInputFile">
                                            <?php
                                            }else{
                                                ?>
                                                <table class='sliding_images'>
                                                    <tr>
                                                        <?php
                                                            
                                                            echo "<td>
                                                                        <div class='outer-slide'>
                                                                        <img class='slides' src='".base_url()."themes/uploads/category/category_".$category_detail['category_id']."/".$thumbnail['image_name']."'>
                                                                            <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$thumbnail['image_id']."/page'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                        </div>
                                                                    </td>";
                                                           
                                                        ?>
                                                    </tr>
                                                </table>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Update Category</button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>

                </section><!-- /.content -->
            </div><!-- /.right-side -->
        </div><!-- ./wrapper -->
