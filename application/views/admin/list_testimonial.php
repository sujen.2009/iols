
        <div id="page-wrapper">

            <div class="container-fluid">

<!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                   Testimonials
                       
                    </h1>
                    <?php
                        echo $this->session->flashdata('message_success');
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-danger">
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Client Name</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($page_list as $key=>$list){
                                            echo '
                                                  <tr>
                                                        <td>'.($key+1).'</td>
                                                        <td>';
                                                        echo ($list["hometext"]==1) ? "<a style='cursor:pointer' href='javascript:void(0)' title='Home page text'><img alt='star' style='height:21px; margin-right:10px;' src='".base_url()."themes/images/star.png'></a>":"";                                                       
                                                         echo $list["page_title"].'</td>
                                                        <td>'.$list["page_updated_date"].'</td>
                                                        <td>
                                                            <a href="'.base_url().'userctrl/edit_testimonial/'.$list["page_id"].'"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></a>
                                                        </td>
                                                        <td>
                                                             <a onclick="return confirm_del(\'Are you sure you want to delete this testimonial?\');" href="'.base_url().'userctrl/testimonial/delete/'.$list["page_id"].'"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></a>
                                                        </td>
                                                    </tr>

                                           '; 

                                    }
                                    ?>
                                  
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Client Name</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div> 
                    </div>    
                </section><!-- /.content -->

                </div>
                </div>