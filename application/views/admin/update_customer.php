
        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Update Customer Info
                        </h1>
                       
                    </div>
                </div>


                 <div class="row">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_customer'); ?>'>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <input type='hidden' name='contact_id' value='<?php echo $customer_detail["contact_id"]; ?>'>
                                            <input required type="text" placeholder="" value="<?php echo $customer_detail['fullname'] ?>" name='fullname' class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Address</label>
                                             <input required type="text" placeholder="" value="<?php echo $customer_detail['address'] ?>" name='address' class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Contact no.</label>
                                             <input required type="text" placeholder="" value="<?php echo $customer_detail['contactno'] ?>" name='contactno' class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Email address</label>
                                             <input required type="email" placeholder="" value="<?php echo $customer_detail['email_address'] ?>" name='email_address' class="form-control">
                                        </div>
 <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <?php
                                                if(sizeof($thumbnail)==0){
                                            ?>
                                                <input type="file" name='thumb_image' id="exampleInputFile">
                                            <?php
                                            }else{
                                                ?>
                                                <table class='sliding_images'>
                                                    <tr>
                                                        <?php
                                                            
                                                            echo "<td>
                                                                        <div class='outer-slide'>
                                                                        <img class='slides' src='".base_url()."themes/uploads/page/page_".$page_detail['page_id']."/".$thumbnail['image_name']."'>
                                                                            <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$thumbnail['image_id']."/page'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                        </div>
                                                                    </td>";
                                                           
                                                        ?>
                                                    </tr>
                                                </table>
                                            <?php
                                            }
                                            ?>
                                        </div>

                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Update Customer</button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
