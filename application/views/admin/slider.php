<script> 
    $(function() {
        var count=0;
        $('#file_upload').uploadify({
            'swf'      : '<?php echo base_url(); ?>themes/js/uploadify/uploadify.swf',
            'uploader' : '<?php echo base_url(); ?>themes/js/uploadify/uploadify.php',
            'fileExt'        : '*.jpg;*.gif;*.png',
            'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
            'multi'     : true,
            'onUploadComplete' : function(file) {
                $('.btnsubmit').show();
                $('#uploadedImages .imageclear').before('<li  style="width:200px;"><img style="width:100% !important;" src="<?php echo base_url(); ?>themes/temp/'+file.name+'"><input type="hidden" name="image['+count+'][name]" value="'+file.name+'"><br /><input placeholder="Caption" name="image['+count+'][caption]" type="text" /></li>');
              //  alert('The file ' + file.name + ' finished processing.');
              $('#gallery_image_size').val(count);
              count++;
            }

            // Put your options here
        });
        $('.lang_select').hide();
    });
</script>





        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Sliding Images
                        </h1>
                       
                    </div>
                </div>


                 <div class="row galleryimagelist">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                   
                                    <table>
                                            <tr>
                                                <td>
                                                Select Images
                                                </td>
                                                <td>
                                                :
                                                </td>
                                                <td>
                                                    <input type='file' id='file_upload'>
                                                </td>
                                            </tr>
                                    </table>
                            <form method='post' action='<?php echo base_url(); ?>userctrl/updateSlider'>                                
                                <ul id='uploadedImages'>
                                    <div class='clear imageclear'></div>
                                    <li>
                                    <input type='hidden' name='gallery_image_size' id='gallery_image_size'>
                                    <button style="display: none;" class="btn btn-primary btn-lg btnsubmit" type="submit">Submit images</button>
                                    </li>
                                    <div class='clear'>
                                </ul>

                            </form>


                            <div class="row sliderimgs">
                                <?php 
                                    foreach($sliders as $imgs){
                                        echo "<div class='col-md-3'>";
                                        echo "<img title='".$imgs['slider_caption']."' src='".base_url()."themes/temp/".$imgs['image_name']."'>";
                                        echo " <a href='".base_url()."userctrl/edit_slider_image/".$imgs['slider_id']."'><button class='btn btn-primary btn-xs btnedit' data-title='Edit' data-toggle='modal' data-target='#edit'><span class='glyphicon glyphicon-pencil'></span></button></a>
                                            <a onclick='return  confirm_del(\"Are you sure you want to delete this image?\");' href='".base_url()."userctrl/del_slider_image/".$imgs['slider_id']."'><button class='btn btn-danger btn-xs btndel' data-title='Delete' data-toggle='modal' data-target='#delete' ><span class='glyphicon glyphicon-trash'></span></button></a>";
                                        echo "</div>";
                                    }
                                ?>
                            </div>
                           
                            <div class='clear'></div>
                            </ul>   
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>
        
      <style>
      #uploadedImages input{
        width:100%;
        margin-bottom:10px;
      }  
      #uploadedImages li,.galleryimagelist li{
        list-style-type:none;
        float:left;
        margin-left:50px;
        width:200px;


      }

      .galleryimagelist li{
        position:relative;
      }
      .galleryimagelist button.btnedit{
        position:absolute;
        bottom:10px; 
        right:20px;
      }  
      .galleryimagelist button.btndel{
        position:absolute;
        bottom:10px; 
        right:0px;
      }
      .galleryimagelist img{
            margin-bottom:10px;

      }



      .sliderimgs img{
        max-width:100%;
        height: 200px;
      }
      </style>