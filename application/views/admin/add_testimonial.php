
        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Testimonial
                        </h1>
                       
                    </div>
                </div>


                 <div class="row">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_page'); ?>'>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>
                                            <input type="hidden" name="page_type" value="testimonial">
                                            Client Name</label>
                                            <input required type="text" placeholder="" name='page_title' class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>
                                            Message
                                            </label>
                                            <textarea placeholder="" name='page_description'  class="form-control tinymce"></textarea>
                                        </div>
                                         

                                        <div class="form-group">
                                            <label for="exampleInputFile">Client Image</label>
                                            <input type="file" name='thumb_image' id="exampleInputFile">
                                        </div>

                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Add Testimonial</button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
