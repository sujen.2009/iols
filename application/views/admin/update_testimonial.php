<?php
    if($page_detail['hometext']==1)
    {

        ?>
        <script>
            $(document).ready(function(){

                $('.chk_imp').attr('checked','checked');
            });
        
        </script>
        <?php
    }

?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update <?php echo $type; ?>
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_page'); ?>'>
                                        <!-- text input -->
                                        <input type="hidden" name="page_type" value="<?php echo $type; ?>">
                                        <div class="form-group">
                                            <label>
                                                Client Name
                                            </label>
                                             <input type='hidden' name='page_id', value='<?php echo $page_detail["page_id"]; ?>'>
                                            <input required type="text" placeholder="" value='<?php echo set_value("page_title",$page_detail["page_title"]) ?>' name='page_title' class="form-control">
                                        </div>

                                   

                                        <div class="form-group">
                                            <label>
                                            Message
                                            </label>
                                            <textarea placeholder="" name='page_description'  class="form-control tinymce"><?php echo set_value("page_description",$page_detail["page_description"]) ?></textarea>
                                        </div>
                                        <?php
                                            if($type!="news"){
                                        ?>
                                        
                                        <?php 
                                        }?>
                                         <div class="form-group">
                                            <label for="exampleInputFile">Client Image</label>
                                            <?php
                                                if(sizeof($thumbnail)==0){
                                            ?>
                                                <input type="file" name='thumb_image' id="exampleInputFile">
                                            <?php
                                            }else{
                                                ?>
                                                <table class='sliding_images'>
                                                    <tr>
                                                        <?php
                                                            
                                                            echo "<td>
                                                                        <div class='outer-slide'>
                                                                        <img style='max-height:200px; max-width:200px;' class='slides' src='".base_url()."themes/uploads/page/page_".$page_detail['page_id']."/".$thumbnail['image_name']."'>
                                                                            <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$thumbnail['image_id']."/testimonial'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                        </div>
                                                                    </td>";
                                                           
                                                        ?>
                                                    </tr>
                                                </table>
                                            <?php
                                            }
                                            ?>
                                        </div>

                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Update Testimonial</button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>

                </section><!-- /.content -->
            </div>
            </div>
     