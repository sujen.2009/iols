   <!-- /#wrapper -->
    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>themes/js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->

        <script type="text/JavaScript">
        
                    var confirm_del=function(confirm_msg){

                        if(confirm(confirm_msg)){
                            return true;
                        }else{
                            return false;

                        }
                    }

                      $(document).ready(function(){

                        $('#example1').dataTable()

                        $('textarea.tinymce').tinymce({
                            // Location of TinyMCE script
                            script_url : '<?php echo base_url() ?>themes/js/tinymce/jscripts/tiny_mce/tiny_mce.js',

                            // General options
                            theme : "advanced",
                            plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
                            language : "en",
                            // Theme options
                            theme_advanced_buttons1 : "html,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,jbimages,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                            //theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                           // theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                           
                            theme_advanced_toolbar_location : "top",
                            theme_advanced_toolbar_align : "left",
                            theme_advanced_statusbar_location : "bottom",
                            theme_advanced_resizing : true,

                        });

                    });
        </script>>

</body>

</html>
