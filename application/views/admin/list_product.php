            <!-- Right side column. Contains the navbar and content of the product -->
    <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Content Header (product header) -->
                <section class="content-header">
                    <h1>
                       Products
                    </h1>
                    <?php
                        echo $this->session->flashdata('message_success');
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-danger">
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>product Name</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($product_list as $key=>$list){
                                            echo '
                                                  <tr>
                                                        <td>'.($key+1).'</td>
                                                        <td>';
                                                        // echo ($list["hometext"]==1) ? "":"";                                                       
                                                         echo $list["product_title"].'</td>
                                                        <td>'.$list["updated_date"].'</td>
                                                        <td>
                                                            <a href="'.base_url().'userctrl/product/edit/'.$list["product_id"].'"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></a>
                                                        </td>
                                                        <td>
                                                             <a onclick="return confirm_del(\'Are you sure you want to delete this product?\');" href="'.base_url().'userctrl/product/delete/'.$list["product_id"].'"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></a>
                                                        </td>
                                                    </tr>

                                           '; 

                                    }
                                    ?>
                                  
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>product Name</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div> 
                    </div>    
                </section><!-- /.content -->
            </div><!-- /.right-side -->
            </div><!-- /.right-side -->
      