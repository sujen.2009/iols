
        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add <?php echo $type; ?>
                        </h1>
                       
                    </div>
                </div>


                 <div class="row">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_page'); ?>'>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>
                                            <input type="hidden" name="page_type" value="<?php echo $type; ?>">
                                            <?php
                                            if(strtolower($type)=="news"){
                                                echo "News title";
                                            }else{
                                                echo "Page Name";
                                            }

                                            ?></label>
                                            <input required type="text" placeholder="" name='page_title' class="form-control">
                                        </div>

                                        <!-- textarea -->
                                        <div class="form-group">
                                            <label>Meta Description</label>
                                            <textarea placeholder=""  name='page_meta_desc' class="form-control"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Meta Tags</label> <small>Separate Tags with comma ' , '</small> 
                                            <textarea placeholder="" name='page_meta_tags' class="form-control"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Short description</label>
                                            <textarea placeholder="" name='page_short_description'  class="form-control tinymce"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>
                                            <?php
                                            if(strtolower($type)=="news"){
                                                echo "News Description";
                                            }else{
                                                echo "Page content";
                                            }
                                            ?>
                                            </label>
                                            <textarea placeholder="" name='page_description'  class="form-control tinymce"></textarea>
                                        </div>
                                         <?php
                                            if(strtolower($type)!="news"){
                                                ?>
                                                    <div class="form-group">
                                                        <label><input type='hidden' name='hometext' value='0'>
                                                        <input class='chk_imp' type='checkbox' name='hometext' value='1'> Home page text</label>
                                                        
                                                    </div>

                                                <?php
                                            }
                                        ?>
<!-- 
                                        <div class="form-group">
                                            <label for="exampleInputFile">Sliding Images <?php echo form_error('Pages_image'); ?></label>
                                            <input  type="file" name='image1' id="exampleInputFile">
                                            <input  type="file" name='image2' id="exampleInputFile">
                                            <input  type="file" name='image3' id="exampleInputFile">
                                            <input  type="file" name='image4' id="exampleInputFile">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <input type="file" name='thumbnailimage' id="exampleInputFile">
                                        </div> -->

                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Add <?php echo $type ?></button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
