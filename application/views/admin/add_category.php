
          
        <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Category
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-warning">
                                <div class="box-body">
                                    <form role="form" enctype="multipart/form-data" method='post' action='<?php echo base_url('userctrl/add_new_category'); ?>'>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Category Name</label>
                                            <input required type="text" name='category_title' placeholder="Category title" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Parent Category</label>
                                         
                                            <select name='category_parent' class="form-control">
                                                <option value='0'>NULL</option>
                                               <?php

                                               foreach($parent_categories as $cat){

                                                    echo "<option value='".$cat['category_id']."'>".$cat['category_title']."</option>";
                                               }
                                               ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">Sliding Images <?php echo form_error('Pages_image'); ?></label>
                                            <input  type="file" name='image1' id="exampleInputFile">
                                            <input  type="file" name='image2' id="exampleInputFile">
                                            <input  type="file" name='image3' id="exampleInputFile">
                                            <input  type="file" name='image4' id="exampleInputFile">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <input type="file" name='thumbnailimage' id="exampleInputFile">
                                        </div>

                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Add Category</button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>

                </section><!-- /.content -->
            </div><!-- /.right-side -->
        </div><!-- ./wrapper -->
