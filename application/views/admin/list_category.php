
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List Category
                    </h1>
                     <?php
                        echo $this->session->flashdata('message_success');
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-warning">
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Category Name</th>
                                        <th>Parent Category</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                           foreach($category_list as $key=>$catlist){
                                                echo '
                                                         <tr>
                                                            <td>'.($key+1).'</td>
                                                            <td>'.$catlist["category_title"].'</td>
                                                            <td>'.$catlist["parentcat_title"].'</td> 
                                                            <td>'.$catlist["category_updated_date"].'</td>
                                                            <td>
                                                                 <a href="'.base_url().'userctrl/category/edit/'.$catlist["category_id"].'">
                                                                     <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button>
                                                                </a>             
                                                            </td>
                                                            <td>
                                                                 <a onclick="return confirm_del(\'Are you sure you want to delete this category?\');" href="'.base_url().'userctrl/category/delete/'.$catlist["category_id"].'">
                                                                    <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                ';

                                           } 

                                    ?>
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Category Name</th>
                                        <th>Parent Category</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div> 
                    </div>    
                </section><!-- /.content -->
            </div><!-- /.right-side -->
        </div><!-- ./wrapper -->
