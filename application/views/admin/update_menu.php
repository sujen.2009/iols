

        <div id="page-wrapper">

            <div class="container-fluid">

             <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Menu management
                        </h1>
                       
                    </div>
                </div>


                 <div class="row">
                    <div class="col-lg-12">
                         <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                          
                                        <div style='float:left; width:50%'>
                                             <div>
                                                <h4>Add page</h4>
                                                 <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url("userctrl/new_menu/page"); ?>'>
                                                    
                                                    <div class="form-group">
                                                        <label>Select page</label>
                                                        <select  class="form-control" name='menu_pageid'>
                                                                <option value='0'>
                                                                    SELECT
                                                                </option>
                                                                <?php
                                                                    foreach($page_list as $page){
                                                                        echo "<option value='".$page['page_id']."'>".$page['page_title']."</option>";
                                                                    }
                                                                ?>
                                                            </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Select page</label>
                                                        <select name='menu_parentid'  class="form-control">
                                                                <option value='0'>
                                                                    None
                                                                </option>
                                                                 <?php
                                                                        foreach($menu_tree as $tree){
                                                                            echo "<option value='".$tree['menu_id']."'>";
                                                                                if($tree['menu_title']!="")
                                                                                    echo $tree['menu_title'];
                                                                                else
                                                                                    echo $tree['page_title'];
                                                                            echo "</option>";
                                                                            if(sizeof($tree['childs'])>0)
                                                                                generateChildMenuForOption($tree['childs']);

                                                                            echo "</li>";

                                                                        }
                                                                    ?>
                                                            </select>
                                                    </div>

                                                    <div class="buttons container-fluid row">
                                                        <div class="pull-right">
                                                            <button class="btn btn-primary btn-lg" type="submit">Add menu</button>
                                                        </div>
                                                    </div>
                                               
                                                </form>
                                             </div>
                                            
                                             <div>
                                                <h4>Add link</h4>
                                                 <form method='post' action='<?php echo base_url("userctrl/new_menu/link"); ?>'>
                                                    <div class="form-group">
                                                        <label> Page link[ http:// compulsary ]</label>
                                                        <input required  class="form-control" type='text' name='menu_link'>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Page title</label>
                                                        <input required  class="form-control" type='text' name='menu_title'>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Parent menu</label>
                                                         <select  class="form-control" name='menu_parentid'>
                                                                <option value='0'>
                                                                    None
                                                                </option>
                                                                 <?php
                                                                        foreach($menu_tree as $tree){
                                                                            echo "<option value='".$tree['menu_id']."'>";
                                                                                if($tree['menu_title']!="")
                                                                                    echo $tree['menu_title'];
                                                                                else
                                                                                    echo $tree['page_title'];
                                                                            echo "</option>";
                                                                            if(sizeof($tree['childs'])>0)
                                                                                generateChildMenuForOption($tree['childs']);

                                                                            echo "</li>";

                                                                        }
                                                                    ?>
                                                            </select>
                                                    </div>

                                                    
                                                    <div class="buttons container-fluid row">
                                                        <div class="pull-right">
                                                            <button class="btn btn-primary btn-lg" type="submit">Add menu</button>
                                                        </div>
                                                    </div>
                                                </form>
                                             </div>
                                            

                                        </div>
                                        <div style='float:left; width:50%'>
                                            <h4>Menu tree</h4>
                                            <ul class="menu-tree">
                                                <?php
                                                    foreach($menu_tree as $key=>$tree){
                                                        if($tree['menu_title']!=""){
                                                            echo "<li>".$tree['menu_title'];
                                                            echo '<a title="edit" href="'.base_url('userctrl/edit_menu/'.$tree['menu_id']).'"><i class="fa fa-edit"></i></a>';                                                       
                                                        }
                                                        else if(isset($tree['page_title']) && $tree['page_title']!="")
                                                            echo "<li>".$tree['page_title'];
                                                        else 
                                                            echo "<li>".$tree['trip_name'];
                                                        if($key!=0)
                                                            echo '<a title="Move up" href="'.base_url('userctrl/sort_menu_up/'.$tree['menu_id']).'"><i class="fa fa-caret-square-o-up"></i></a>';                                                       
                                                        if(($key+1)!=sizeof($menu_tree))
                                                            echo '<a title="Move down" href="'.base_url('userctrl/sort_menu_down/'.$tree['menu_id']).'"><i class="fa fa-caret-square-o-down"></i></a>';                                                       
                                                        if(sizeof($tree['childs'])>0)
                                                            generateChildMenu($tree['childs']);
                                                        else
                                                             echo '<a title="Delete" onclick="return confimationtext();" href="'.base_url('userctrl/delete_menu/'.$tree['menu_id']).'"><i class="fa fa-close"></i></a>';

                                                        echo "</li>";

                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                        <div class='clear'></div>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                    </div>
                </div>
               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
























<?php
    function generateChildMenu($menu_tree){
        echo "<ul>";
         foreach($menu_tree as $key=>$tree){
            if($tree['menu_title']!=""){
                echo "<li>".$tree['menu_title'];
                echo '<a title="Edit" href="'.base_url('userctrl/edit_menu/'.$tree['menu_id']).'"><i class="fa fa-edit"></i></a>';                                                       
            }
            else if(isset($tree['page_title']) && $tree['page_title']!="")
                echo "<li>".$tree['page_title'];
            else 
                echo "<li>".$tree['trip_name'];
           
            if($key!=0)
                echo '<a title="Move up" href="'.base_url('userctrl/sort_menu_up/'.$tree['menu_id']).'"><i class="fa fa-caret-square-o-up"></i></a>';
            if(($key+1)!=sizeof($menu_tree))
                echo '<a title="Move down" href="'.base_url('userctrl/sort_menu_down/'.$tree['menu_id']).'"><i class="fa fa-caret-square-o-down"></i></a>';
            if(sizeof($tree['childs'])>0)
                generateChildMenu($tree['childs']);
            else
                 echo '<a title="Delete" onclick="return confimationtext();" href="'.base_url('userctrl/delete_menu/'.$tree['menu_id']).'"><i class="fa fa-close"></i></a>';
            echo "</li>";

        }
        echo "</ul>";

    }
 function generateChildMenuForOption($menu_tree){
       
         foreach($menu_tree as $tree){
            echo "<option value='".$tree['menu_id']."'>";
            if($tree['menu_title']!="")
                echo $tree['menu_title'];
             else if(isset($tree['page_title']) && $tree['page_title']!="")
                echo "<li>".$tree['page_title'];
            else 
                echo "<li>".$tree['trip_name'];
           
            echo "</option>";
            if(sizeof($tree['childs'])>0)
                generateChildMenuForOption($tree['childs']);

          
        }
       
    }

?>

<style type="text/css">
    .menu-tree i{
        margin-left: 10px;
    }
</style>