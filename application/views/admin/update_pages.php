<?php
    if($page_detail['hometext']==1)
    {

        ?>
        <script>
            $(document).ready(function(){

                $('.chk_imp').attr('checked','checked');
            });
        
        </script>
        <?php
    }

?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update <?php echo $type; ?>
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="col-md-12">
                            <!-- general form elements disabled -->
                            <div class="box box-danger">
                                <div class="box-body">
                                    <form  enctype='multipart/form-data' role="form" method='post' action='<?php echo base_url('userctrl/add_new_page'); ?>'>
                                        <!-- text input -->
                                        <input type="hidden" name="page_type" value="<?php echo $type; ?>">
                                        <div class="form-group">
                                            <label>
                                                <?php
                                                if($type=="news"){
                                                    echo"News Title";
                                                }else{
                                                    echo "Page Title";    
                                                }
                                                ?>

                                            </label>
                                             <input type='hidden' name='page_id', value='<?php echo $page_detail["page_id"]; ?>'>
                                            <input required type="text" placeholder="" value='<?php echo set_value("page_title",$page_detail["page_title"]) ?>' name='page_title' class="form-control">
                                        </div>

                                        <!-- textarea -->
                                        <div class="form-group">
                                            <label>Meta Description</label>
                                            <textarea placeholder=""  name='page_meta_desc' class="form-control"><?php echo set_value("page_meta_desc",$page_detail["page_meta_desc"]) ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Meta Tags</label> <small>Separate Tags with comma ' , '</small> 
                                            <textarea placeholder="" name='page_meta_tags' class="form-control"><?php echo set_value("page_meta_tags",$page_detail["page_meta_tags"]) ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Short description</label>
                                            <textarea placeholder="" name='page_short_description'  class="form-control tinymce"><?php echo set_value("page_short_description",$page_detail["page_short_description"]) ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>
                                            <?php
                                                if($type=="news"){
                                                    echo"News Description";
                                                }else{
                                                    echo "Page content";    
                                                }
                                                ?>

                                            </label>
                                            <textarea placeholder="" name='page_description'  class="form-control tinymce"><?php echo set_value("page_description",$page_detail["page_description"]) ?></textarea>
                                        </div>
                                        <?php
                                            if($type!="news"){
                                        ?>
                                        <div class="form-group">
                                            <label><input type='hidden' name='hometext' value='0'><input class='chk_imp' type='checkbox' name='hometext' value='1'> Home page text 
                                            .[ Current <i><?php echo $currentHomePage['page_title'] ?></i> ]</label>
                                            
                                        </div>
                                        <?php 
                                        }?>
                                        <!--  <div class="form-group">               
                                            <div id='image_process' class="form-group">
                                         <!-- 
                                            <br /><span class='note'> [ Image should be less than 3 MB ]</span>
                                        
                                            <label for="exampleInputFile">Sliding Images <?php echo form_error('country_image'); ?></label>
                                            <?php
                                                $total=4-sizeof($slide_images);
                                                for($i=1;$i<=$total;$i++){
                                                    echo '
                                                        <input  type="file" name="image'.$i.'" id="exampleInputFile">
                                                    ';
                                                }
                                            ?>
                                           
                                        </div> -->
                                      <!--   <?php
                                                if(sizeof($slide_images)!=0){
                                            ?>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Uploaded images :</label>
                                            <table class='sliding_images'>
                                                <tr>
                                                    <?php
                                                        foreach($slide_images as $img){
                                                                echo "<td>
                                                                            <div class='outer-slide'>
                                                                            <img class='slides' src='".base_url()."themes/uploads/page/page_".$page_detail['page_id']."/".$img['image_name']."'>
                                                                                <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$img['image_id']."/page'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                            </div>
                                                                        </td>";
                                                        }
                                                    ?>
                                                </tr>
                                            </table>
                                        </div>
                                        <?php
                                               }
                                            ?> -->
                                       <!--  <div class="form-group">
                                            <label for="exampleInputFile">Thumbnail Image</label>
                                            <?php
                                                if(sizeof($thumbnail)==0){
                                            ?>
                                                <input type="file" name='thumb_image' id="exampleInputFile">
                                            <?php
                                            }else{
                                                ?>
                                                <table class='sliding_images'>
                                                    <tr>
                                                        <?php
                                                            
                                                            echo "<td>
                                                                        <div class='outer-slide'>
                                                                        <img class='slides' src='".base_url()."themes/uploads/page/page_".$page_detail['page_id']."/".$thumbnail['image_name']."'>
                                                                            <div class='btn btn-danger btn-xs btn-del' data-title='Delete' data-toggle='modal' data-target='#delete'><a onclick='return confimationtext();' href='".base_url()."userctrl/del_image/".$thumbnail['image_id']."/page'><span class='glyphicon glyphicon-trash'></span></a></div>
                                                                        </div>
                                                                    </td>";
                                                           
                                                        ?>
                                                    </tr>
                                                </table>
                                            <?php
                                            }
                                            ?>
                                        </div>
 -->
                                        <div class="buttons container-fluid row">
                                            <div class="pull-right">
                                            <button class="btn btn-danger btn-lg" type="reset">Reset</button>
                                            <button class="btn btn-primary btn-lg" type="submit">Update <?php echo $type; ?></button>
                                        </div>
                                    </div>
                                        
                                    </form>
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>

                </section><!-- /.content -->
            </div>
            </div>
     