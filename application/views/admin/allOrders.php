
        <div id="page-wrapper">

            <div class="container-fluid">

<!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Orders
                    </h1>
                    <?php
                        echo $this->session->flashdata('message_success');
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-danger">
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Ordered by</th>
                                        <th>Ordered date</th>
                                        <th>Total Cost</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($orders as $key=>$list){
                                            echo '
                                                  <tr data-order_id="'.$list["order_id"].'">
                                                        <td>'.($key+1).'</td>
                                                        <td>';
                                                         echo $list["order_placed_by"].'</td>
                                                        <td>'.$list["created_date"].'</td>
                                                        <td>
                                                        $'.$list["total_price"].'
                                                        </td>
                                                        <td>
                                                            <a title="View detail" href="javascript:"><button class="btn btn-primary btn-xs item_detail" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon glyphicon-align-justify"></span></button></a>
                                                             <a title="Delete" onclick="return confirm_del(\'Are you sure you want to delete this order?\');" href="'.base_url().'userctrl/delete_order/'.$list["order_id"].'"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></a>
                                                        </td>
                                                    </tr>

                                           '; 

                                    }
                                    ?>
                                  
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S/N</th>
                                        <th>page Name</th>
                                        <th>Last Edited</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div> 
                    </div>    
                </section><!-- /.content -->

                </div>
                </div>



<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Order detail</h4>
      </div>
      <div class="modal-body">
                <div class="row">
                     <div class="col-lg-12">
                            <div class="row  order_items">
                                <table class="table">
                                    <tr><td>Order placed by </td><td>: </td><td><span class='placed_by'></span></td></tr>
                                    <tr><td>Total cost </td><td>: </td><td><span class='total_cost'></span></td></tr>
                                    <tr><td>Ordered Date </td><td>:</td><td> <span class='orderDate'></td></span></tr>

                                </table>
                                
                            </div>
                     </div>
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script>
var getheadings=function(){

    html='<tr> \
                                        <th>\
                                            S.N.\
                                        </th>\
                                        \
                                        <th>\
                                            Order by\
                                        </th>\
                                        <th>\
                                            Total price\
                                        </th>\
                                        <th>\
                                            Action\
                                        </th>\
                                    </tr>';
                                    return html;
}


var itemdatatable="";

var bindEvent=function(){
    $('.item_detail').on('click',function(){
        
        var orderid=$(this).parents('tr').attr("data-order_id");
        $.get('<?php echo base_url(); ?>userctrl/getOrderDetail/'+orderid,function(data){
            var heading=getOrderHeadings();
            $('#myModal').modal('show');

            heading=getOrderHeadings();
            $('.order_items table.order_detail').remove();
            $('.order_items').append("<table class='datatable order_detail table table-striped table-bordered table-hover'><thead>"+heading+"</thead><tbody></tbody></table>");
            data=JSON.parse(data);
            html=[];

            $('.placed_by').text(data.detail.order_placed_by);
            
            $('.total_cost').text("$"+data.detail.total_price);
            $('.orderDate').text(data.detail.created_date);
            data.orderItems.forEach(function(x,i){
                html.push("<tr>");
                html.push("<td>"+(i+1)+"</td>");
                html.push("<td>"+x.product_title+"</td>");
                html.push("<td>$"+x.product_rate+"</td>");
                html.push("<td>"+x.product_quantity+"</td>");
                html.push("<td>$"+(x.product_quantity*x.product_rate)+"</td>");
                html.push("</tr>");
            })
            $('.order_items table.order_detail tbody').html(html.join(""));
        });
        
    })
}


var getOrderHeadings=function(){
    html=[];
    html.push("<tr>");
    html.push("<td>S.N.</td>");
    html.push("<td>Item</td>");
    html.push("<td>Rate</td>");
    html.push("<td>Quantity</td>");
    html.push("<td>Total</td></tr>");
    return html.join("");
}
bindEvent();
</script>