<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IOLS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>themes/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>themes/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>themes/css/site.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>themes/css/notifIt.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>themes/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="<?php echo base_url(); ?>themes/css/sb-admin.css" rel="stylesheet"> -->

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url(); ?>themes/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>themes/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>themes/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/flip.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/notifIt.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.bootstrap.newsbox.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.validate.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
</head>
<body>

<div class="pageContainer container-fluid">
    


    <header>
                <div class="row logoSection">
                    <div class="col-lg-12">

                        <div class="container">
                            <img src="<?php echo base_url(); ?>themes/images/logo.jpg">

                        </div>

                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-12 menuSection">   
                        
                        <nav class="navbar navbar-default">
                            <div class="container">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                </div>

                                <div class="collapse navbar-collapse" style="position:relative" id="bs-example-navbar-collapse-1">
                                  <ul class="nav navbar-nav">
                                    <li class=""><a href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a></li>
                                   
                                    <?php

                                    foreach($menu_tree as $key=>$tree){
                                        $menu_title="";
                                        if(sizeof($tree['childs'])>0)
                                            echo "<li class='dropdown'>";
                                        else
                                            echo "<li>";
                                        $link="";
                                        if($tree['menu_title']!=""){
                                            $menu_title= $tree['menu_title'];
                                            $link=$tree['menu_link'];
                                         
                                        }
                                        else if(isset($tree['page_title']) && $tree['page_title']!=""){
                                            $link=base_url()."pages/".urlencode(str_replace(' ','_',$tree['page_title']));
                                            $menu_title= "".$tree['page_title'];
                                        }
                                        else 
                                            echo "<li><a href='".base_url()."trip/".urldecode($tree['trip_name'])."'>".$tree['trip_name'];
                                                         
                                        if(sizeof($tree['childs'])>0){
                                            echo ' <a href="'.$link.'" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$menu_title.' <span class="caret"></span></a>';
                                            generateChildMenu($tree['childs']);
                                        }else{

                                            echo "<a href='".$link."'>".$menu_title."</a>";
                                        }
                                       
                                        echo "</li>";

                                    }
                                ?>


                                    
                                  </ul>
                                  <form method="post" action="<?php echo base_url(); ?>search">
                                  <i class="fa fa-search searchProductIcon" aria-hidden="true"></i>
                                   <input type="text" class="searchProduct" name="searchKey">
                                   </form>
                                <!--   <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Link</a></li>
                                  </ul> -->
                                </div>
                            </div>
                        </nav>
                    </div>    
                </div>    
    </header>



<?php
    function generateChildMenu($menu_tree){
        echo "<ul class='dropdown-menu'>";
         foreach($menu_tree as $key=>$tree){
            if($tree['menu_title']!=""){
                echo "<li><a href='".$tree['menu_link']."''>".$tree['menu_title'];
                                                           
            }
            else if(isset($tree['page_title']) && $tree['page_title']!="")

                echo "<li><a href='#'><a href='".base_url()."pages/".urlencode(str_replace(' ','_',$tree['page_title']))."'>".$tree['page_title'];
            else 
                echo "<li><a href='".base_url()."trip/".urldecode($tree['trip_name'])."'>".$tree['trip_name'];
           
            echo "</a>";
            if(sizeof($tree['childs'])>0){
                
                generateChildMenu($tree['childs']);
            }
           
            echo "</li>";

        }
        echo "</ul>";

    }
?>



    <script>
            $(function(){
                var current=window.location.href;
                $('.navbar-nav a').each(function(){
                    if($(this).attr('href')==current){

                        if($(this).parents('ul').hasClass('collapse')){
                            $(this).parents('ul').addClass('in');
                            $(this).parents('ul').parents('li').addClass('active');
                        }else{
                            $(this).parents('li').addClass('active');
                        }

                         $(this).addClass('active');
                    }
                })
            })

    </script>