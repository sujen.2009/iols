<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct(){
		parent::__construct();
		$this->load->model('main_model');
		if($this->session->userdata('user_login')==true)
				redirect(base_url()."Userctrl");
		
	}
	
	public function index(){
		
		$this->load->view('login/login');
		
	}
	
	public function login_process(){
			
			$uname=$this->input->post('userid');
			$pass=$this->input->post('password');
			
			$pass=hash('sha1',$pass);
			$where=array('user_name'=>$uname,'user_password'=>$pass);

		
			$check=$this->main_model->getFromDb('tbl_users',$where,'row');
			if(sizeof($check)>1){
					$detail=$check;
					$this->session->set_userdata('user_login',true);
					//$this->session->set_userdata('user_detail',$detail);
					$this->session->set_userdata('user_type','admin');
					redirect(base_url()."userctrl");
					return;
			}
			
			$this->session->set_flashdata('login_error','<div class="validation_error">Username or password is not correct.</div>');
			redirect(base_url()."login");
			return;
			
	}
	
	
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
