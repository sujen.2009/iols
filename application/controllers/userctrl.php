<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userctrl extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	function __construct(){
		parent::__construct();
		if($this->session->userdata('user_login')==false){
				redirect(base_url()."login");
				return;
		}

		$this->load->model('main_model');

	}

	public function index()
	{
		$this->load->view('admin/header');
		$this->load->view('admin/dashboard');
		$this->load->view('admin/footer');
	}

	public function add_content($type="page"){

		$data['type']=$type;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/add_content');
		$this->load->view('admin/footer');
	}

	public function add_testimonial(){

		$this->load->view('admin/header');
		$this->load->view('admin/add_testimonial');
		$this->load->view('admin/footer');
	}



	public function list_page($type='page'){

		$data['toopen']="menu_content";
		$data['type']=$type;
		$data['uploaded_path']="";
		$where=Array('page_type'=>$type);
		$data['page_list']=$this->main_model->getFromDb('tbl_pages',$where,'','','page_id',"DESC");
		

		
		
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/list_page');
		$this->load->view('admin/footer');
	}

	public function list_testimonial(){

		$data['toopen']="menu_content";
		$data['uploaded_path']="";
		$where=Array('page_type'=>'testimonial');
		$data['page_list']=$this->main_model->getFromDb('tbl_pages',$where,'','','page_id',"DESC");

		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/list_testimonial');
		$this->load->view('admin/footer');
	}


	public function edit_page($page_id,$type=""){
		$this->db->select('count(*) as totalcount');
		$where=Array('hometext'=>1);
		$data['homePageCount']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		$data['currentHomePage']=$this->main_model->getFromDb('tbl_pages',$where,'row');

		$data['type']=$type;
		$data['toopen']="menu_content";
		
		$where=Array('page_id'=>$page_id);
		$data['page_detail']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		
		$where=Array('image_primary_id'=>$page_id,'image_type'=>'page','image_thumb_name'=>0);
		$data['slide_images']=$this->main_model->getFromDb('tbl_images',$where);

		$where=Array('image_primary_id'=>$page_id,'image_type'=>'page','image_thumb_name'=>1);
		$data['thumbnail']=$this->main_model->getFromDb('tbl_images',$where,'row');

		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/update_pages');
		$this->load->view('admin/footer');
	}
	

	public function edit_testimonial($page_id){
		$this->db->select('count(*) as totalcount');
		$where=Array('hometext'=>1);
		$data['homePageCount']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		$data['currentHomePage']=$this->main_model->getFromDb('tbl_pages',$where,'row');

		$data['type']='testimonial';
		$data['toopen']="menu_content";
		
		$where=Array('page_id'=>$page_id);
		$data['page_detail']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		
		$where=Array('image_primary_id'=>$page_id,'image_type'=>'page','image_thumb_name'=>0);
		$data['slide_images']=$this->main_model->getFromDb('tbl_images',$where);

		$where=Array('image_primary_id'=>$page_id,'image_type'=>'page','image_thumb_name'=>1);
		$data['thumbnail']=$this->main_model->getFromDb('tbl_images',$where,'row');

		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/update_testimonial');
		$this->load->view('admin/footer');
	}
	


	public function new_menu($menu_type){

		$menu=$this->input->post();
		if(!isset($menu['menu_pageid']) || $menu['menu_pageid']==0)
				$menu['menu_pageid']=NULL;
		if($menu['menu_parentid']==0)
				$menu['menu_parentid']=NULL;
		$this->db->select('MAX(menu_order) as totalcount');
		$where=Array('menu_parentid'=>$menu['menu_parentid']);
		$menu_order=$this->main_model->getFromDb('tbl_menu',$where,'row');

		$menu['menu_order']=$menu_order['totalcount']+1;
		$menu['menu_status']=1;
		$menu['menu_updated_date']=date('Y-m-d h:i:s');
		$menu['menu_added_date']=date('Y-m-d h:i:s');
$this->session->set_flashdata('message_success',"<span class='msg_success'>Menu added successfully</span>");
		$this->main_model->addToDb('tbl_menu',$menu);
		redirect(base_url('userctrl/menu_mgmt'));


	}


	
	public function update_menu(){
//$menu_type
		$menu=$this->input->post();
		if(!isset($menu['menu_pageid']) || $menu['menu_pageid']==0)
				$menu['menu_pageid']=NULL;
		if($menu['menu_parentid']==0)
				$menu['menu_parentid']=NULL;
		
		$menu['menu_status']=1;
		$menu['menu_updated_date']=date('Y-m-d h:i:s');
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Menu updated successfully</span>");
		$this->main_model->updateDB('tbl_menu','menu_id',$this->input->post('menu_id'),$menu);
		redirect(base_url('userctrl/menu_mgmt'));


	}


	private function getMenuChilds($parentmenu){
		$this->db->select('m.*,p.page_title');
		$where=Array('menu_parentid'=>$parentmenu);
		$result=$this->main_model->getFromDb('tbl_menu m LEFT OUTER JOIN tbl_pages p on (m.menu_pageid=p.page_id)',$where,'','','menu_order','ASC');
		//print_r($result);
		//echo ($this->db->last_query());
		foreach($result as $key=>$res){
				$result[$key]['childs']=$this->getMenuChilds($res['menu_id']);
		}
		return $result;
	}



	public function add_new_page(){

		$type=$this->input->post('page_type');
		if($type!='testimonial'){

			if(!$this->form_validation->run('add_new_page')){
				
					$this->form_validation->set_error_delimiters('<span class="validation_error"> [  ',' ] </span>');
					if(!$this->input->post('page_id') || $this->input->post('page_id')==""){
							$this->add_content();
					}else{
							$this->edit_page($this->input->post('page_id'));					
					}
					return;
			}
		}

		$toadd=$this->input->post();
		if($type=="")
			$type="page";
		$toadd['page_updated_date']=date('Y-m-d h:i:s');
		if(!$this->input->post('page_id') || $this->input->post('page_id')==""){
			
			$toadd['page_type']=$type;
			$toadd['page_added_date']=date('Y-m-d h:i:s');
			$toadd['page_status']=1;
			$page_id=$this->main_model->addToDb('tbl_pages',$toadd);
			$this->uploadImage('page',$page_id);
			$this->session->set_flashdata('message_success',"<span class='msg_success'>".ucfirst($type)." added successfully</span>");
		}else{

			if($this->input->post('hometext')==1){
				$update['hometext']=0;
				$this->main_model->updateDB('tbl_pages','',"",$update);
			}

			$toadd['page_type']=$type;
			$page_id=$this->input->post('page_id');
			$this->main_model->updateDB('tbl_pages','page_id',$this->input->post('page_id'),$toadd);
			$this->uploadImage('page',$this->input->post('page_id'));
			$this->session->set_flashdata('message_success',"<span class='msg_success'>".ucfirst($type)." updated successfully</span>");
		}
		
		redirect(base_url('Userctrl/list_'.$type));
	}



	public function list_customer(){

		$data['toopen']="menu_content";
		
		$data['uploaded_path']="";
		$data['page_list']=$this->main_model->getFromDb('tbl_contact_info','','','','contact_id',"DESC");
		

		
		
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/list_customer');
		$this->load->view('admin/footer');
	}

	public function add_new_customer(){

		$toadd=$this->input->post();
		
		if(!$this->input->post('contact_id') || $this->input->post('contact_id')==""){

			$toadd['added_date']=date('Y-m-d h:i:s');
			$toadd['status']=1;
			$contact_id=$this->main_model->addToDb('tbl_contact_info',$toadd);
			$this->uploadImage('contact',$contact_id);
			$this->session->set_flashdata('message_success',"<span class='msg_success'>Customer added successfully</span>");
		}else{

			$page_id=$this->input->post('contact_id');
			$this->main_model->updateDB('tbl_contact_info','contact_id',$this->input->post('contact_id'),$toadd);
			$this->uploadImage('contact',$this->input->post('contact_id'));
			$this->session->set_flashdata('message_success',"<span class='msg_success'>Customer updated successfully</span>");
		}
		
		redirect(base_url('Userctrl/list_customer'));
	}




	public function edit_customer($contact_id){
	
		$where=Array('contact_id'=>$contact_id);
		$data['customer_detail']=$this->main_model->getFromDb('tbl_contact_info',$where,'row');

		$where=Array('image_primary_id'=>$contact_id,'image_type'=>'contact','image_thumb_name'=>1);
		$data['thumbnail']=$this->main_model->getFromDb('tbl_images',$where,'row');

		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/update_customer');
		$this->load->view('admin/footer');
	}

	public function del_image($imageid,$type){

		//$type=$this->input->get('type');
		//$imageid=$this->input->get('imageid');
		$where=Array('image_id'=>$imageid);
		$image_detail=$this->main_model->getFromDb('tbl_images',$where,'row');
		$file=config_item('absolute_url')."themes/".$type."/".$image_detail['image_name'];
		if(file_exists($file)){
			unlink($file);
		}

		$where=Array('image_id'=>$imageid,'image_type'=>$type);
		$this->main_model->deleteFromDb('tbl_images','image_id',$imageid);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>page image deleted successfully</span>");
		redirect(base_url()."userctrl/".$type."/edit/".$image_detail['image_primary_id']."#image_process");

	}


	public function delete_page($page_id,$type='page'){
		
		$this->session->set_userdata('form_message','page deleted successfully!!!');
		$data['page_list']=$this->main_model->deleteFromDb('tbl_pages','page_id',$page_id);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>page deleted successfully</span>");
		redirect(base_url("userctrl/".$type."/list"));

	}
	public function delete_contact($contact_id){
		
		$this->session->set_userdata('form_message','Customer deleted successfully!!!');
		$data['contact_list']=$this->main_model->deleteFromDb('tbl_contact_info','contact_id',$contact_id);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Customer deleted successfully</span>");
		redirect(base_url("userctrl/list_customer"));

	}

	public function delete_order($order_id){
		
		$this->session->set_userdata('form_message','Order deleted successfully!!!');
		$data['contact_list']=$this->main_model->deleteFromDb('tbl_order','order_id',$order_id);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Order deleted successfully</span>");
		redirect(base_url("userctrl/allOrders"));

	}


	private function uploadImage($imagetype,$id){
		//print_r($_FILES);
		ini_set('memory_limit', '96M');
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		
		$target_file=config_item('absolute_url')."themes/uploads/".$imagetype;
		if(!file_exists($target_file)){
			mkdir($target_file,0777);	
		}
		$target_file=config_item('absolute_url')."themes/uploads/".$imagetype."/".$imagetype."_".$id."/";
		if(!file_exists($target_file)){
			mkdir($target_file,0777);	
		}
		for($i=1;$i<=4;$i++){
			if(isset($_FILES['image'.$i])){
				if($_FILES['image'.$i]['name'] != ""){
					//echo $target_file;
					$name = time()."_".$_FILES["image".$i]["name"];
					move_uploaded_file($_FILES["image".$i]["tmp_name"],$target_file.$name);
					//echo $_FILES['image'.$i]['name']; 
					$data['image_name']=$name;
					$data['image_primary_id']=$id;
					$data['image_type']=$imagetype;
					$data['image_added_date']=date("Y-m-d h:i:s");
					$data['image_status']=1;
					$this->main_model->addToDb('tbl_images',$data);

				}
			}
		}
		if(isset($_FILES['thumb_image']))
		if($_FILES['thumb_image']['name'] != ""){
				$name = time()."_thumb_".$_FILES["thumb_image"]["name"];
				move_uploaded_file($_FILES["thumb_image"]["tmp_name"],$target_file.$name);
				
				$data['image_name']=$name;
				$data['image_primary_id']=$id;
				$data['image_type']=$imagetype;
				$data['image_thumb_name']=1;
				$data['image_added_date']=date("Y-m-d h:i:s");
				$data['image_status']=1;
				$this->main_model->addToDb('tbl_images',$data);

		}

		if(isset($_FILES['thumb_image1']))
		if($_FILES['thumb_image1']['name'] != ""){
				$name = time()."_thumb_".$_FILES["thumb_image1"]["name"];
				move_uploaded_file($_FILES["thumb_image1"]["tmp_name"],$target_file.$name);
				
				$data['image_name']=$name;
				$data['image_primary_id']=$id;
				$data['image_type']=$imagetype."_thumb1";
				$data['image_thumb_name']=1;
				$data['image_added_date']=date("Y-m-d h:i:s");
				$data['image_status']=1;
				$this->main_model->addToDb('tbl_images',$data);

		}

		if(isset($_FILES['thumb_image2']))
		if($_FILES['thumb_image2']['name'] != ""){
				$name = time()."_thumb_".$_FILES["thumb_image2"]["name"];
				move_uploaded_file($_FILES["thumb_image2"]["tmp_name"],$target_file.$name);
				
				$data['image_name']=$name;
				$data['image_primary_id']=$id;
				$data['image_type']=$imagetype."_thumb2";
				$data['image_thumb_name']=1;
				$data['image_added_date']=date("Y-m-d h:i:s");
				$data['image_status']=1;
				$this->main_model->addToDb('tbl_images',$data);

		}

	}



	public function list_product(){
		
		$data['toopen']="menu_product";
		
		$data['uploaded_path']="";
		$data['product_list']=$this->main_model->getFromDb('tbl_products','','','','product_id',"DESC");
		
		
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/list_product');
		$this->load->view('admin/footer');
	}


	public function edit_product($product_id){
	
		$data['toopen']="menu_product";
		
		$where=Array('product_id'=>$product_id);
		$data['product_detail']=$this->main_model->getFromDb('tbl_products',$where,'row');
		$data['product_detail']['product_price']=json_decode($data['product_detail']['product_price']);
		$data['product_detail']['product_unit']=json_decode($data['product_detail']['product_unit']);
		$where=Array('image_primary_id'=>$product_id,'image_type'=>'product','image_thumb_name'=>0);
		$data['slide_images']=$this->main_model->getFromDb('tbl_images',$where);

		$where=Array('image_primary_id'=>$product_id,'image_type'=>'product','image_thumb_name'=>1);
		$data['thumbnail']=$this->main_model->getFromDb('tbl_images',$where,'row');
		$data['categories']=$this->main_model->execute_raw_query('Select * from tbl_categories','');
		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/update_product');
		$this->load->view('admin/footer');
	}


	public function delete_product($product_id){
		
		$this->session->set_userdata('form_message','product deleted successfully!!!');
		$this->main_model->deleteFromDb('tbl_products','product_id',$product_id);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>product deleted successfully</span>");
		redirect(base_url("userctrl/product/list"));

	}


	public function list_category(){
		
//$this->db->select('count(*) as totalpage');
		
		$data['uploaded_path']="";
		$this->db->select('a.*,b.category_title as parentcat_title');
		$this->db->join('tbl_categories as b', ' b.category_id=a.category_parent','left outer');
		$data['category_list']=$this->main_model->getFromDb('tbl_categories as a','','','','a.category_id','desc');
		// echo $this->db-last_query();
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/list_category');
		$this->load->view('admin/footer');
	}


	public function add_category(){
$this->db->select('count(*) as totalpage');
		
		$data['parent_categories']=$this->main_model->execute_raw_query("SELECT * FROM tbl_categories where category_parent IS NULL",'result');
$data['toopen']="menu_category";
		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/add_category');
		$this->load->view('admin/footer');
	}

	public function add_new_category(){

		$toadd=$this->input->post();
		
		if($this->input->post('category_parent')==0)
			$toadd['category_parent']=NULL;

		$toadd['category_updated_date']=date('Y-m-d h:i:s');
		if(!$this->input->post('category_id') || $this->input->post('category_id')==""){
			$toadd['category_added_date']=date('Y-m-d h:i:s');
			$toadd['category_status']=1;
			$category_id=$this->main_model->addToDb('tbl_categories',$toadd);
			$this->session->set_flashdata('message_success',"<span class='msg_success'>Category added successfully</span>");
		}else{
			$category_id=$this->input->post('category_id');
			$this->main_model->updateDB('tbl_categories','category_id',$this->input->post('category_id'),$toadd);
			$this->session->set_flashdata('message_success',"<span class='msg_success'>Category updated successfully</span>");
		}
		$this->uploadImage('category',$category_id);
		redirect(base_url('Userctrl/category/list'));

	}



	public function edit_category($category_id){
		
		
		$where=Array('image_primary_id'=>$category_id,'image_type'=>'category','image_thumb_name'=>0);
		$data['slide_images']=$this->main_model->getFromDb('tbl_images',$where);

		
		$where=Array('image_primary_id'=>$category_id,'image_type'=>'category','image_thumb_name'=>1);
		$data['thumbnail']=$this->main_model->getFromDb('tbl_images',$where,'row');

		$data['parent_categories']=$this->main_model->execute_raw_query("SELECT * FROM tbl_categories where category_parent IS NULL",'result');
		$where=Array('category_id'=>$category_id);
		$data['category_detail']=$this->main_model->getFromDb("tbl_categories",$where,'row');
		$data['toopen']="menu_category";
		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/update_category');
		$this->load->view('admin/footer');
	}

	public function delete_category($category_id){
		
		$where=Array('product_type'=>$category_id);
		$this->db->select('count(*) as totalcount');
		$productCheck=$this->main_model->getFromDb('tbl_products',$where,'row');

		if($productCheck['totalcount']>0){
			$this->session->set_flashdata('message_success','<span class="msg_error">Some products are already there in selected Category. Delete products and try again!!!!</span>');
			redirect(base_url("userctrl/category/list"));
			return;	
		}

		$this->session->set_userdata('form_message','Category deleted successfully!!!');
		$this->main_model->deleteFromDb('tbl_categories','category_id',$category_id);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Category deleted successfully</span>");
		redirect(base_url("userctrl/category/list"));

	}


	public function add_new_product(){
		$toadd=$this->input->post();
	
		$toadd['added_date']=date('Y-m-d h:i:s');
		$toadd['product_price']=json_encode($toadd['product_price']);
		$toadd['product_unit']=json_encode($toadd['product_unit']);
		$toadd['added_by']=1;
		$toadd['status']=1;


		if(!$this->input->post('product_id') || $this->input->post('product_id')==""){
			$toadd['updated_date']=date('Y-m-d h:i:s');
			$product_id=$this->main_model->addToDb('tbl_products',$toadd);
		}else{
			$product_id=$this->input->post('product_id');
			$this->main_model->updateDB('tbl_products','product_id',$product_id,$toadd);

		}
	
		$this->uploadImage('product',$product_id);

		redirect(base_url('Userctrl/product/list'));
	}

	public function add_product(){
		$data['toopen']="menu_product";
			
		$data['categories']=$this->main_model->execute_raw_query('Select * from tbl_categories','');
		
		$data['uploaded_path']="";
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/add_products');
		$this->load->view('admin/footer');
	}


	private function unlinktempfiles(){
			$dir = new DirectoryIterator(config_item('absolute_path')."themes/uploads/temp_images");
			foreach ($dir as $fileinfo) {
			    if (!$fileinfo->isDot()) {
			        unlink(config_item('absolute_path').$fileinfo->getFilename());
			    }
			}
	}

	public function add_customer(){


		$this->load->view('admin/header');
		$this->load->view('admin/add_customer');
		$this->load->view('admin/footer');

	}


	public function logout(){
		
		$this->session->sess_destroy();
		redirect(base_url().'login');
	}


	public function menu_mgmt(){

		$this->db->select('count(*) as totalcount');
		$where=Array('hometext'=>1);
		$data['homePageCount']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		//$data['toopen']="menu_content";
		$data['uploaded_path']="";
		//$this->db->where('menu_pageid !=', 1);
		//$this->db->select('menu_pageid');
		$previously_added_menu=$this->main_model->execute_raw_query('SELECT menu_pageid FROM tbl_menu where menu_pageid Is NOT NULL','result');
		$simparr=$this->changeToSimpleArray($previously_added_menu);
		if(sizeof($simparr)>0)
			$this->db->where_not_in('page_id', $simparr);

		$data['page_list']=$this->main_model->getFromDb('tbl_pages','','','','page_id',"DESC");

		$data['menu_tree']=$this->getMenuTree();
		
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/update_menu');
		$this->load->view('admin/footer');

	}


	public function changeToSimpleArray($array){

		$arr=Array();
		foreach($array as $a){

			foreach($a as $b){
				array_push($arr, $b);

			}

		}
		return $arr;

	}


	private function getMenuTree(){

		$result=$this->main_model->execute_raw_query('SELECT m.*,p.page_title FROM tbl_menu m LEFT OUTER JOIN tbl_pages p on (m.menu_pageid=p.page_id) where menu_parentid Is NULL order by menu_order ASC','result');
		foreach($result as $key=>$res){
				$result[$key]['childs']=$this->getMenuChilds($res['menu_id']);
		}
		return $result;
	}



	public function sort_menu_up($menu_id){
		$where=Array('menu_id'=>$menu_id);
		$menu_parent=$this->main_model->getFromDb('tbl_menu',$where,'row');
		//print_r($menu_parent);
		if($menu_parent['menu_parentid']!=""){
			$menu=$this->main_model->execute_raw_query("SELECT * FROM tbl_menu where menu_parentid IN (SELECT menu_parentid FROM tbl_menu where menu_id=".$menu_id.") and menu_order < (SELECT menu_order FROM tbl_menu where menu_id=".$menu_id.") order by menu_order DESC",'row');
			//echo $this->db->last_query();
			//print_r($menu);
	
		}else{

			$menu=$this->main_model->execute_raw_query("SELECT * FROM tbl_menu where menu_parentid IS NULL and menu_order < (SELECT menu_order FROM tbl_menu where menu_id=".$menu_id.") order by menu_order DESC",'row');
			//echo $this->db->last_query();
			//print_r($menu);
			
		}
		//echo $this->db->Last_query();
		//print_r($menu);
		$prevorder=$menu_parent['menu_order'];
		$neworder=$menu['menu_order'];
		
		//echo $prevorder." ".$menu_parent['menu_id'];
		//echo $neworder.' '.$menu['menu_id'];
		//return;
		$update['menu_order']=$neworder;
		$this->main_model->updateDB('tbl_menu','menu_id',$menu_parent['menu_id'],$update);
		$update['menu_order']=$prevorder;
		$this->main_model->updateDB('tbl_menu','menu_id',$menu['menu_id'],$update);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Menu sorted successfully</span>");
		redirect(base_url("userctrl/menu_mgmt"));
	}
	public function sort_menu_down($menu_id){
		$where=Array('menu_id'=>$menu_id);
		$menu_parent=$this->main_model->getFromDb('tbl_menu',$where,'row');
		//print_r($menu_parent);
		if($menu_parent['menu_parentid']!=""){
			$menu=$this->main_model->execute_raw_query("SELECT * FROM tbl_menu where menu_parentid IN (SELECT menu_parentid FROM tbl_menu where menu_id=".$menu_id.") and menu_order > (SELECT menu_order FROM tbl_menu where menu_id=".$menu_id.") order by menu_order ASC",'row');
			//echo $this->db->last_query();
			//print_r($menu);
	
		}else{

			$menu=$this->main_model->execute_raw_query("SELECT * FROM tbl_menu where menu_parentid IS NULL and menu_order > (SELECT menu_order FROM tbl_menu where menu_id=".$menu_id.") order by menu_order ASC",'row');
			//echo $this->db->last_query();
			//print_r($menu);
			
		}
		//echo $this->db->Last_query();
		//print_r($menu);
		$prevorder=$menu_parent['menu_order'];
		$neworder=$menu['menu_order'];
		
		//echo $prevorder." ".$menu_parent['menu_id'];
		//echo $neworder.' '.$menu['menu_id'];
		//return;
		$update['menu_order']=$neworder;
		$this->main_model->updateDB('tbl_menu','menu_id',$menu_parent['menu_id'],$update);
		$update['menu_order']=$prevorder;
		$this->main_model->updateDB('tbl_menu','menu_id',$menu['menu_id'],$update);
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Menu sorted successfully</span>");
		redirect(base_url("userctrl/menu_mgmt"));
	}



	public function edit_menu($menu_id){

		$this->db->select('count(*) as totalcount');
		$where=Array('hometext'=>1);
		$data['homePageCount']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		//$data['toopen']="menu_content";
		$data['uploaded_path']="";
		//$this->db->where('menu_pageid !=', 1);
		//$this->db->select('menu_pageid');
		$previously_added_menu=$this->main_model->execute_raw_query('SELECT menu_pageid FROM tbl_menu where menu_pageid Is NOT NULL','result');
		$simparr=$this->changeToSimpleArray($previously_added_menu);
		if(sizeof($simparr)>0)
			$this->db->where_not_in('page_id', $simparr);

		$data['page_list']=$this->main_model->getFromDb('tbl_pages','','','','page_id',"DESC");
		$data['menu_tree']=$this->getMenuTree();
		
		$where=Array('menu_id'=>$menu_id);
		$data['menu_detail']=$this->main_model->getFromDb('tbl_menu',$where,'row');
		if($data['menu_detail']['menu_parentid']=="")
				$data['menu_detail']['menu_parentid']=0;
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/edit_menu');
		$this->load->view('admin/footer');

	}

	
	public function delete_menu($menu_id){

		$this->main_model->deleteFromDb('tbl_menu','menu_id',$menu_id);
		//echo $this->db->last_query();
		$this->session->set_flashdata('message_success',"<span class='msg_success'>Menu deleted successfully</span>");
		redirect(base_url("userctrl/menu_mgmt"));

	}


	public function allOrders(){

		$data['uploaded_path']="";
		$data['orders']=$this->main_model->getFromDb('tbl_order','','','','order_id',"DESC");
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/allOrders');
		$this->load->view('admin/footer');
	}


	public function slider(){

		$data['toopen']="";

		$data['uploaded_path']="";
		$where=Array('image_type'=>'slider');
		$data['sliders']=$this->main_model->getFromDb('tbl_slider INNER JOIN tbl_images ON (tbl_images.image_primary_id=tbl_slider.slider_id)',$where,'');
		
		
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/slider');
		$this->load->view('admin/footer');


	}

	public function updateSlider(){
		echo "<pre>";
		print_r($this->input->post());
		echo "</pre>";
		$images=$this->input->post('image');
		foreach ($images as $key => $img) {
			$toadd=Array();
			$toadd['slider_title']=$img['caption'];
			$toadd['slider_caption']=$img['caption'];
			$toadd['status']=1;
			$sliderid=$this->main_model->addToDb('tbl_slider',$toadd);
			
			$img_entry=Array();
			$img_entry['image_name']=$img['name'];
			$img_entry['image_primary_id']=$sliderid;
			$img_entry['image_added_date']=date('Y-m-d h:i:s');
			$img_entry['image_status']=1;
			$img_entry['image_type']='slider';
			$this->main_model->addToDb('tbl_images',$img_entry);
		}
		redirect(base_url('userctrl/slider'));
	}

	public function getOrderDetail($orderid){

		$where=['order_id'=>$orderid];
		$this->db->select('*,o.product_price as product_rate');
		$this->db->join('tbl_products as b','o.product_id=b.product_id');
		$data['orderItems']=$this->main_model->getFromDb('tbl_order_detail o',$where);
		$data['detail']=$this->main_model->getFromDb('tbl_order',$where,'row');
		echo json_encode($data);

	}

	public function edit_slider_image($image_id){

		$data['toopen']="";
		
		$data['uploaded_path']="";
		$where=Array('slider_id'=>$image_id, 'image_type'=>'slider');
		$data['imgs']=$this->main_model->getFromDb('tbl_slider INNER JOIN tbl_images ON (tbl_images.image_primary_id=tbl_slider.slider_id)',$where,'row');
		
		
		$this->load->view('admin/header',$data);
		//~ $this->load->view('admin/sidebar');
		$this->load->view('admin/slider_images_edit');
		$this->load->view('admin/footer');


	}


	public function updateSliderImage(){

		// print_r($_POST);
		// return;
	
		$where=Array('slider_id'=>$this->input->post('slider_id'));
		$gallery_images=$this->main_model->getFromDb('tbl_slider',$where,'row');
		


		$image['slider_caption']=$this->input->post('caption');
	
		// $image['added_date']=date("Y-m-d h:i:s");
		// $image['status']=1;
		$img_id=$this->main_model->updateDB('tbl_slider','slider_id',$this->input->post('slider_id'),$image);

		$this->session->set_flashdata('message_success',"<span class='msg_success'>Slider updated successfully</span>");
		redirect(base_url()."userctrl/slider");
	}



	public function del_slider_image($image_id){

	
		$data['uploaded_path']="";
		$where=Array('slider_id'=>$image_id);
		$gallery_images=$this->main_model->getFromDb('tbl_slider',$where,'row');
		$where=Array('image_primary_id'=>$image_id);
		$this->main_model->execute_raw_query_nonreturn("DELETE FROM tbl_images where image_primary_id='.$image_id.' and image_type='slider'");

		$this->main_model->deleteFromDb('tbl_slider','slider_id',$image_id);

		$this->session->set_flashdata('message_success',"<span class='msg_success'>Slider image deleted successfully</span>");
		redirect(base_url()."userctrl/slider");
	}



	

}
