<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $globaldata;

	function __construct(){
		parent::__construct();
		

		$this->load->model('main_model');
		$this->initiateGlobalDatas();
	}


	private function initiateGlobalDatas(){
		$this->globaldata['menu_tree']=$this->getMenuTree();
		$where=Array('image_type'=>'slider');
		$this->globaldata['sliders']=$this->main_model->getFromDb('tbl_slider INNER JOIN tbl_images ON (tbl_images.image_primary_id=tbl_slider.slider_id)',$where);
		
		$where=Array('page_type'=>'news');
		$this->globaldata['news']=$this->main_model->getFromDb('tbl_pages',$where,'','','page_id','desc');
		
	}


	public function index()
	{
		$data=$this->globaldata;
		$data['products']=$this->main_model->getFromDb('tbl_products','','',6);
		foreach($data['products'] as $key=>$product){

			$where=Array('image_primary_id'=>$product['product_id'],'image_type'=>'product','image_thumb_name'=>1);
			$data['products'][$key]['slide_images']=$this->main_model->getFromDb('tbl_images',$where,'row');

			// echo $this->db->last_query()
		}
		// 		echo "<pre>";
		// print_r($data);
		// die();


		$where=Array('page_type'=>'page','hometext'=>1);
		$data['welcometext']=$this->main_model->getFromDb('tbl_pages',$where,'row');
		


		$where=Array('page_type'=>'testimonial');
		$data['testimonials']=$this->main_model->getFromDb('tbl_pages',$where,'',4,'page_id','desc');
		
		foreach($data['testimonials'] as $key=>$testimonial){

			$where=Array('image_primary_id'=>$testimonial['page_id'],'image_type'=>'page','image_thumb_name'=>1);
			$data['testimonials'][$key]['image']=$this->main_model->getFromDb('tbl_images',$where,'row');

			// echo $this->db->last_query()
		}

		$this->load->view('header',$data);
		$this->load->view('index');
		$this->load->view('footer');
	}

	public function orderProduct(){

		$data=$this->globaldata;
		$data['products']=$this->main_model->getFromDb('tbl_products');
		$this->load->view('header',$data);
		$this->load->view('order');
		$this->load->view('footer');
	}


	private function getMenuTree(){

		$result=$this->main_model->execute_raw_query('SELECT m.*,p.page_title FROM tbl_menu m LEFT OUTER JOIN tbl_pages p on (m.menu_pageid=p.page_id) where menu_parentid Is NULL order by menu_order ASC','result');
		foreach($result as $key=>$res){
				$result[$key]['childs']=$this->getMenuChilds($res['menu_id']);
		}
		return $result;
	}
	


	private function getMenuChilds($parentmenu){
		$this->db->select('m.*,p.page_title');
		$where=Array('menu_parentid'=>$parentmenu);
		$result=$this->main_model->getFromDb('tbl_menu m LEFT OUTER JOIN tbl_pages p on (m.menu_pageid=p.page_id)',$where,'','','menu_order','ASC');
		//print_r($result);
	//	echo ($this->db->last_query());
		foreach($result as $key=>$res){
				$result[$key]['childs']=$this->getMenuChilds($res['menu_id']);
		}
		return $result;
	}

	
	public function product_detail($product_title){
	
		$data=$this->globaldata;
			$product_title=urldecode(str_replace('_', ' ', $product_title));
			
			// $this->db->select('*,tbl_category.category_title as category_url');
			$data['categories']=$this->main_model->getFromDb('tbl_categories');
			
				



			$where=Array('product_title'=>$product_title);
			$data['product_detail']=$this->main_model->getFromDb('tbl_products INNER JOIN tbl_categories ON (tbl_categories.category_id=tbl_products.product_type) ',$where,'row');
			// print_r($pdt);
			$where=Array('image_primary_id'=>$data['product_detail']['product_id'],'image_type'=>'product');
			$data['image']=$this->main_model->getFromDb('tbl_images',$where,'row');


			$data['products']=$this->main_model->getFromDb('tbl_products','','',6);
			foreach($data['products'] as $key=>$product){

				$where=Array('image_primary_id'=>$product['product_id'],'image_type'=>'product','image_thumb_name'=>1);
				$data['products'][$key]['slide_images']=$this->main_model->getFromDb('tbl_images',$where,'row');

				// echo $this->db->last_query()
			}
			// echo $this->db->last_query();
			// print_r($data['product_detail']);
			
			$this->load->view('header',$data);		
			$this->load->view('product-detail');
			$this->load->view('footer');	

	}

	public function getProductDetail($product_id){
			$totalincart=0;
			$inCart=$this->session->userdata('cart');
			// 	echo "<pre>";
			// print_r($inCart);
			// echo "</pre>";			
			if(sizeof($inCart)>0){
				foreach ($inCart as $key => $product) {
					if($product['product_id']==$product_id){
						$totalincart=$product['quantity'];
					}

				}
			}

			$where=Array('product_id'=>$product_id);
			$product_detail=$this->main_model->getFromDb('tbl_products INNER JOIN tbl_categories ON (tbl_categories.category_id=tbl_products.product_type) ',$where,'row');
		
			$where=Array('image_primary_id'=>$product_detail['product_id'],'image_type'=>'product','image_thumb_name'=>1);
			$product_detail['images']=$this->main_model->getFromDb('tbl_images',$where,'row');
			$product_detail['inCart']=$totalincart;
			echo json_encode($product_detail);
	}

	public function getCategoryChild($category_id){

		$where=Array('category_parent'=>$category_id);
		$categories=$this->main_model->getFromDb('tbl_categories',$where);
		foreach($categories as $key=>$cat){
			$categories[$key]['child']=$this->getCategoryChild($cat['category_id']);
		}
		return $categories;
	}

	public function products($category=""){
		$data=$this->globaldata;
		$where=Array('category_parent'=>NULL);
		$data['categories']=$this->main_model->getFromDb('tbl_categories',$where);
		foreach($data['categories'] as $key=>$cat){
			$data['categories'][$key]['child']=$this->getCategoryChild($cat['category_id']);
		}
		$data['searchkey']="";
		if($category!=""){
			$category=urldecode($category);
			$searchkey=$this->input->post('searchKey');
			$data['selectedCategory']=$category;
			$where=Array('category_title'=>$category);
		}else{
			$searchkey=$this->input->post('searchKey');
			if($searchkey!=""){
				$data['searchkey']=$searchkey;
				$this->db->like('product_title',$searchkey);
			}
			$where=Array();
			$data['selectedCategory']="";
		}
		$this->db->join('tbl_categories', 'category_id = product_type');
		$data['products']=$this->main_model->getFromDb('tbl_products tp',$where);
		foreach($data['products'] as $key=>$product){

			$where=Array('image_primary_id'=>$product['product_id'],'image_type'=>'product','image_thumb_name'=>1);
			$data['products'][$key]['slide_images']=$this->main_model->getFromDb('tbl_images',$where,'row');

			// echo $this->db->last_query()
		}
		// 		echo "<pre>";
		// print_r($data);
		// die();

		$this->load->view('header',$data);
		$this->load->view('allProducts');
		$this->load->view('footer');
	}

	public function pages($page_title){
			$data=$this->globaldata;
			$page_title=str_replace('_',' ',$page_title);
			$page_title=urldecode($page_title);
			$where=Array('page_title'=>$page_title);
			$data['page_detail']=$this->main_model->getFromDb('tbl_pages',$where,'row');

			$where=Array('image_primary_id'=>$data['page_detail']['page_id'],'image_type'=>'page','image_thumb_name'=>0);
			$data['slide_images']=$this->main_model->getFromDb('tbl_images',$where);
			$data['slide_location']='page';
			$data['type']='page';
			$this->load->view('header',$data);		
			$this->load->view('pages');
			$this->load->view('footer');	

	}

	public function checkout(){
			$data=$this->globaldata;	
			$products=Array();
			$inCart=$this->session->userdata('cart');
			// echo "<pre>";
			// print_r($inCart);
			// echo "</pre>";

			if(sizeof($inCart)>0){
				foreach ($inCart as $key => $product) {


					$where=Array('product_id'=>$product['product_id']);
					$product_detail=$this->main_model->getFromDb('tbl_products INNER JOIN tbl_categories ON (tbl_categories.category_id=tbl_products.product_type) ',$where,'row');
					
					$product_detail['selectedUnit']=$product['unit'];
					$product_detail['quantity']=$product['quantity'];
					$product_detail['cart_id']=$product['cart_id'];
					
					$where=Array('image_primary_id'=>$product['product_id'],'image_type'=>'product','image_thumb_name'=>1);
					$product_detail['slide_images']=$this->main_model->getFromDb('tbl_images',$where,'row');



					$products[]=$product_detail;
					
				}

			}
			
			$data['products']=$products;
			$this->load->view('header',$data);		
			$this->load->view('checkout');
			$this->load->view('footer');	

	}

	public function savetocart(){
		$tosave=$this->input->post();
		if($this->session->userdata('cart')){
			$topush=$this->session->userdata('cart');
			$size=sizeof($topush);
			$tosave['cart_id']=$topush[$size-1]['cart_id']+1;
			$topush[]=$tosave;

			$this->session->set_userdata('cart',$topush);	
		}else{
			$topush=Array();
			$tosave['cart_id']=1;
			$topush[]=$tosave;
			$this->session->set_userdata('cart',$topush);
		}

	}

	public function removeFromCart($id){
		$cdata=$this->session->userdata('cart');
			echo "<pre>";
			print_r($cdata);
			echo "</pre>";
			$narray=Array();
			foreach ($cdata as $key => $data) {
					if($data['cart_id']!=$id){
						array_push($narray,$data);
					}
			}
			$this->session->set_userdata('cart',$narray);
			redirect(base_url()."home/checkout");
	}

	// public function send_mail($to,$subject,$message,$from,$reply_to=""){
		
	// $this->load->library('mailin');
	// $mailin = new Mailin();
 // 	$mailin->InitializeMail('sujen.2009@gmail.com', 'fK9sVTIU4B7MFxZR');
	// // // $mailin = new Mailin('sujen.2009@gmail.com', 'fK9sVTIU4B7MFxZR');
	// $mailin->addTo('sujen.2009@gmail.com', 'None')->
	// 		setFrom('sujen.2009@gmail.com', 'None')->
	// 		setReplyTo('sujen.2009@gmail.com','None')->
	// 		setSubject('Enter the subject here')->
	// 		setText('Hello')->
	// 		setHtml('<strong>Hello</strong>');
	// // echo "test";
	// $res = $mailin->send();
	// // print_r($res);

	// }



	public function clearCart(){
		$this->session->set_userdata('cart',[]);
		// echo "cart cleared successfully";
	}

	public function orderPlacement(){

		$products=Array();
		$inCart=$this->session->userdata('cart');

		if(sizeof($inCart)>0){


			$toadd=Array();
			
			$toadd['order_placed_by']=$this->input->post('fullname');
			$toadd['contact_no']=$this->input->post('contact');
			$toadd['shipping_address']=$this->input->post('shipping_address');
			$toadd['email']=$this->input->post('email');
			$toadd['state']=$this->input->post('state');
			$toadd['country']=$this->input->post('country');
			$toadd['created_date']=date("Y-m-d h:i:s");
			$toadd['total_price']=$this->input->post('total_price');
			$toadd['status']=0;
			$this->main_model->addToDB('tbl_order',$toadd);	
			$order_id=$this->db->insert_id();



			foreach ($inCart as $key => $product) {
				$toadd=Array();
				$toadd['product_id']=$product['product_id'];
				$toadd['order_id']=$order_id;
				$toadd['product_unit']=$product['unit'];
				$toadd['product_quantity']=$product['quantity'];
				$where=Array('product_id'=>$product['product_id']);
				$product_detail=$this->main_model->getFromDb('tbl_products INNER JOIN tbl_categories ON (tbl_categories.category_id=tbl_products.product_type) ',$where,'row');
				
				$units=json_decode($product_detail['product_unit']);
				$key = array_search($product['unit'], $units);
				$rate=json_decode($product_detail['product_price']);

				$toadd['product_price']=$rate[$key];
				$this->main_model->addToDB('tbl_order_detail',$toadd);	
				
			}
			$this->clearCart();
			$this->send_mail("sujen.2009@gmail.com","Test message","test","sujen.2009@yahoo.com");		
			echo "Success";
				
		}
	}


	public function news($title){
		$data=$this->globaldata;
		$title=urldecode($title);
		$title=str_replace('_',' ',$title);

		$where=Array('page_type'=>'news','page_title'=>$title);
		$data['page_detail']=$this->main_model->getFromDb('tbl_pages',$where,'row',0,5);
		

		$where=Array('image_primary_id'=>$data['page_detail']['page_id'],'image_type'=>'page','image_thumb_name'=>0);
		$data['slide_images']=$this->main_model->getFromDb('tbl_images',$where);
		$data['slide_location']='page';
		$data['type']='news';
		$this->load->view('header',$data);		
		$this->load->view('pages');
		$this->load->view('footer');	
	}

	public function allnews(){
		$data=$this->globaldata;

		$where=Array('page_type'=>'news');
		$data['news']=$this->main_model->getFromDb('tbl_pages',$where,'','','page_id','desc');
		// echo $this->db->last_query();
		$data['type']='news';
		$this->load->view('header',$data);		
		$this->load->view('newslist');
		$this->load->view('footer');	
	}

	public function alltestimonials(){
		$data=$this->globaldata;

		$where=Array('page_type'=>'testimonial');
		$data['news']=$this->main_model->getFromDb('tbl_pages',$where,'','','page_id','desc');
		

		foreach($data['news'] as $key=>$testimonial){

			$where=Array('image_primary_id'=>$testimonial['page_id'],'image_type'=>'page','image_thumb_name'=>1);
			$data['news'][$key]['image']=$this->main_model->getFromDb('tbl_images',$where,'row');

			// echo $this->db->last_query()
		}
		// echo $this->db->last_query();
		$data['type']='testimonial';
		$this->load->view('header',$data);		
		$this->load->view('testimonials');
		$this->load->view('footer');	
	}

	public function contactus(){
		$data=$this->globaldata;
		// echo $this->db->last_query();
		$data['type']='news';
		$this->load->view('header',$data);		
		$this->load->view('contactus');
		$this->load->view('footer');		
	}

	public function sendContactMail(){

		$to="sujen.2009@gmail.com";
		$subject=$this->input->post('subject');
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$message=$this->input->post('message');
		$this->send_mail($to,$subject,$message,$name,$email);

		$this->session->set_flashdata("message_success","<div class='success'>Your message has been received successfully. Thank you for your valuable feedback.</div>");
		redirect(base_url()."contact");
	}


	public function send_mail($to,$subject,$message,$from,$reply_to=""){
		
		$this->load->library('email');

		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['mailtype'] = 'HTML';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);

		$this->email->from($from);
		$this->email->to($to);
		
		$this->email->subject($subject);
		$this->email->message($message);
		
		if($reply_to!="")
			$this->email->reply_to($reply_to);
		
		if($this->email->send())
			return 1;
		else 
			return 0;
			
	}



	// public function checkoutProducts(){
	// 	print_r($this->session->userdata());
	// }

}

