<?php
class Main_model extends CI_Model{
	
	
	
	function __contsruct(){
		
	parent::__construct();
		
		
	}
	
	public function execute_raw_query($query,$return_type){
		
		$query=$this->db->query($query);
		
		if($return_type=='row')
			return $query->row_array();
		else
			return $query->result_array();
			
		
		
	}

	public function execute_raw_query_nonreturn($query){
		
		$query=$this->db->query($query);
		return $this->db->affected_rows();
			
		
		
	}

	public function addToDb($tablename,$data){

		$this->db->insert($tablename,$data);
		return $this->db->insert_id();
	}
	
	public function deleteFromDb($tablename,$field,$field_id){
				$sql="DELETE FROM ".$tablename." where ".$field."=?";
				$query=$this->db->query($sql,$field_id);
				return $this->db->affected_rows();
	}
	
	public function getFromDb($tablename,$where="",$array_type="",$limit="",$sortby="",$ordertype="",$upper_limit=""){
		
		if($where!="")
			$this->db->where($where);
		
		if($ordertype=="")
			$ordertype="desc";
		
		if($sortby!="")
			$this->db->order_by($sortby, $ordertype);

		
		
		if($limit!="" || $upper_limit!=""){
			if($upper_limit!="")
				$this->db->limit($upper_limit,$limit);
			else
				$this->db->limit($limit);
		}					
		$query=$this->db->get($tablename);
		
		if($array_type=="row")
			return $query->row_array();
		else
			return $query->result_array();
		
	}
	

	
	public function updateDb($tablename,$field,$value,$data){
		if($value=="" && $value==""){


		}else
			$this->db->where($field, $value);
		$this->db->update($tablename, $data); 

		return $this->db->affected_rows();
			
	}
	
}
